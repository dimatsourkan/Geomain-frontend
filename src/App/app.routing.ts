import { RouterModule, Routes } from '@angular/router';
import { WrapperComponent } from "./Components/Wrapper/wrapper.component";
import {MAIN_PAGE_ROUTING} from "./AppModules/MainPage/MainPage.routing";
import {IsActivate} from "./BaseModules/Authorization/CanActivate/IsActivate";

const routes: Routes = [

    {
        path : '',
        canActivate : [ IsActivate ],
        children : [
            {
                path: 'auth',
                loadChildren: './AppModules/AppAuthorization/AppAuthorization.module#AppAuthorizationModule'
            },

            ...MAIN_PAGE_ROUTING,

            {
                path: '',
                component: WrapperComponent,
                children: [

                    {
                        path: 'profile',
                        loadChildren: './AppModules/Profile/Profile.module#ProfileModule'
                    },
                    {
                        path: 'faq',
                        loadChildren: './AppModules/Faq/Faq.module#FaqModule'
                    },
                    {
                        path: 'privacy-policy',
                        loadChildren: './AppModules/Privacy/Privacy.module#PrivacyModule'
                    },
                    {
                        path: 'business-sol',
                        loadChildren: './AppModules/BusinessSol/BusinessSol.module#BusinessSolModule'
                    },
                    {
                        path: 'geomain-name',
                        loadChildren: './AppModules/GeomainInfo/GeomainInfo.module#GeomainInfoModule'
                    },
                    {
                        path: 'who-is',
                        loadChildren: './AppModules/WhoIs/WhoIs.module#WhoIsModule'
                    },
                    {
                        path: 'registrar',
                        loadChildren: './AppModules/Registrar/Registrar.module#RegistrarModule'
                    },
                    {
                        path: 'geomarket',
                        loadChildren: './AppModules/Geomarket/Geomarket.module#GeomarketModule'
                    },

                    {
                        path: '',
                        loadChildren: './AppModules/Geomain/Geomain.module#GeomainModule'
                    },
                ]
            },
        ]
    }

    // {
    //     path: '**',
    //     redirectTo: '/'
    // }
];

export const ROUTING = RouterModule.forRoot(routes);
