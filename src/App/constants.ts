export const CREDENTIALS = 'test:test';

export const MAP_API_KEY   : string = 'AIzaSyDrq9E1xqFnYhxT7G7814ooxn5v-oMHij8';

let URL = '';

switch(process.env.ENV) {
    case 'production' :
        URL = 'https://535lpjjwnl.execute-api.us-east-2.amazonaws.com/geomain-prod'; break;
    case 'test' :
        URL = 'https://cqfxi5bbfj.execute-api.us-east-2.amazonaws.com/geomain-dev'; break;
    default :
        URL = 'https://cqfxi5bbfj.execute-api.us-east-2.amazonaws.com/geomain-dev'; break;
}

export const AUTH_URL = URL;
export const API_URL = URL+'/api';
export const USER_SERVICE_API = 'http://34.240.206.145:8080/api';