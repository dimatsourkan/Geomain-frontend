import {NgModule} from '@angular/core';
import {LineChartDirective} from "./LineChart.directive";



@NgModule({
    imports: [
    ],
    declarations: [
	    LineChartDirective
    ],
    exports: [
	    LineChartDirective
    ],
    providers: []
})
export class LineChartModule{}
