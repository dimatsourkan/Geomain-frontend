import {Directive, ElementRef, Input, OnChanges, OnInit} from "@angular/core";
let Chart = require('./../../../Theme/js/chartJs/Chart.min');

@Directive({
    selector : '[lineChart]'
})

export class LineChartDirective implements OnInit,OnChanges {

    el : any;
	ctx : any;
	lineChart : any;

	@Input() data : any;

    constructor(
        private elem : ElementRef,
    ) {
        this.el = elem.nativeElement;
        this.ctx = this.el.getContext("2d");
    }

    options() {
        return {
            type: 'pie',
            data: {
                datasets: [{
                    data: this.data,
                    backgroundColor: [
                        '#f2f5f7',
                        '#ff0500'
                    ],

                }],
                labels: [
                    "Geomains",
                    "Add-Ons",
                ]
            },
            options: {
                responsive: true
            }
        }
    }

	ngOnInit() {
        this.lineChart = new Chart(this.ctx, this.options());
    }

    ngOnChanges() {

        if(this.lineChart) {
            this.lineChart.data.datasets[0].data = this.data;
            this.lineChart.update();
        }
    }

}
