import {AfterViewInit, Directive, ElementRef, Input} from "@angular/core";
import './../../../../node_modules/slick-carousel/slick/slick.min.js';

declare let $: any;

@Directive({
    selector : '[slick]'
})

export class SlickDirective implements AfterViewInit {

    @Input() options : any = {};

    el : any;

    constructor(private elem : ElementRef) {
        this.el = elem.nativeElement;
    }

    ngAfterViewInit() {
        $(this.el).slick(this.options);
    }

}
