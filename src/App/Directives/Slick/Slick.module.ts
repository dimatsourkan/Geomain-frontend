import {NgModule} from '@angular/core';
import {SlickDirective} from "./Slick.directive";

@NgModule({
    imports: [
    ],
    declarations: [
        SlickDirective,
    ],
    exports: [
        SlickDirective
    ],
    providers: []
})
export class SlickModule{}
