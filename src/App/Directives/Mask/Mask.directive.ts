import { Directive, ElementRef, AfterViewInit, Output, EventEmitter, Input } from '@angular/core';
import * as $ from 'jquery';
import "inputmask/dist/jquery.inputmask.bundle";

@Directive({
    selector : '[inputmask]'
})

export class MaskDirective implements AfterViewInit {

    el : any;

    value : string;

    @Input() options : any = {};

    @Output() change : EventEmitter<any> = new EventEmitter<any>();

    constructor(private elem : ElementRef) {
        this.el = elem.nativeElement;
    }

    ngAfterViewInit() {
        let $el : any = $(this.el);
        $el.inputmask(this.options);
        $el.on('change', () => {
            this.change.emit($el.val());
        })
    }

}
