import {NgModule} from '@angular/core';
import {MaskDirective} from "./Mask.directive";



@NgModule({
    imports: [
    ],
    declarations: [
        MaskDirective,
    ],
    exports: [
        MaskDirective
    ],
    providers: []
})
export class MaskModule{}
