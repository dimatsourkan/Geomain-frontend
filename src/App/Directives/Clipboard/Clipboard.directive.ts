import {Directive, ElementRef, AfterViewInit, Input, EventEmitter, Output, OnDestroy} from "@angular/core";
let Clipboard = require('../../../../node_modules/clipboard/dist/clipboard');

@Directive({
    selector : '[clipboard]'
})

export class ClipboardDirective implements AfterViewInit, OnDestroy {

    el : any;

    clipboard : any;

    @Input('text') text : string;

    @Input('element') element : any;

    @Output() onCopy : EventEmitter<any> = new EventEmitter<any>();

    constructor(
        private elem : ElementRef
    ) {
        this.el = elem.nativeElement;
    }

    ngAfterViewInit() {
        this.clipboard = new Clipboard(this.el, {
            // target : () => {
            //     return this.element;
            // },
            text : () => {
                if(this.element) {
                    return this.element.innerText;
                }
                else {
                    return this.text;
                }
	        }
        });

        this.clipboard.on('success', () => {
            this.onCopy.emit();
        })
    }

    ngOnDestroy() {
        this.clipboard.destroy();
    }

}
