import {NgModule} from '@angular/core';
import {ClipboardDirective} from "./Clipboard.directive";



@NgModule({
    imports: [
    ],
    declarations: [
        ClipboardDirective,
    ],
    exports: [
        ClipboardDirective
    ],
    providers: []
})
export class ClipboardModule{}
