export class Helpers {

    loadImage(image: string) {
        return require('./../../Theme/img/' + image);
    }

    getMyLocation(callback : any, errCallback ?: any) {
        if (navigator.geolocation) {
            return navigator.geolocation.getCurrentPosition(callback, errCallback);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }

    togglePass(input : any) {
        if(input.getAttribute('type') == 'password') {
            input.setAttribute('type', 'text');
        }
        else {
            input.setAttribute('type', 'password');
        }
    }

}