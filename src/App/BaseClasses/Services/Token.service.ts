import { Injectable } from '@angular/core';
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class TokenService {

    private TOKEN_KEY = 'token';
    private REFRESH_TOKEN = 'refresh';

    static token() {
        return localStorage.getItem('token');
    }

    /**
     * Установка токена
     * @param token - Строка с токеном
     * @param refresh - Строка с токеном
     */
    setTokens(token: string, refresh : string) {
        localStorage.setItem(this.TOKEN_KEY, token);
        localStorage.setItem(this.REFRESH_TOKEN, refresh);
    }

    /**
     * Получить текущий токен
     */
    getToken() {
        return localStorage.getItem(this.TOKEN_KEY);
    }

    /**
     * Получить текущий токен
     */
    getRefreshToken() {
        return localStorage.getItem(this.REFRESH_TOKEN);
    }

    /**
     * Удалить токен
     */
    resetToken() {
        localStorage.removeItem(this.TOKEN_KEY);
        localStorage.removeItem(this.REFRESH_TOKEN);
    }

    /**
     * Добавить токен в хедеры запросов
     * @param headers
     */
    addToHeader(headers: HttpHeaders) {
        if(this.getToken()) {
            headers.append('Authorization', `Bearer ${this.getToken()}`);
        }
    }

}
