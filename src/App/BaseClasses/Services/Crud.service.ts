import {Injectable} from '@angular/core';
import {IModel} from "../Models/Base.model";
import {IResult, IResultList} from "../Models/Result.model";
import {API_URL} from "../../constants";
import {HttpClient, HttpHeaders} from "@angular/common/http";

/**
 * Абстрактный CRUD Сервис для работы с сервером
 * Его должны унаследовать все crud сервисы
 */
@Injectable()
export class CRUDService<T extends IModel> {

    /**
     * Хедеры которые будут оправляться со всеми запросами
     */
    protected headers: HttpHeaders;

    /**
     * Урл на который круд отправляет запросы
     */
    protected crudUrl: string = '';

    /**
     * Инициализация
     * @param url - Строка вида 'user' по которому должен работать круд
     * @param http - Http объект для работы с сервером
     */
    constructor(
        protected url: string,
        protected http: HttpClient

    ) {

        this.headers = new HttpHeaders();

        this.setCrudUrl(url);
        this.crudUrl = `${API_URL}/${url}`;
        this.headers.append('Accept', 'application/json');

    }

    /**
     * Метод для установки url
     * @param url - Строка вида 'user' по которому должен работать круд
     */
    setCrudUrl(url: any = '') : string {
        return `${this.crudUrl}/${url}`;
    }

    /**
     * Обработка ответа от сервера.
     * Можно преобразовать ответ до того как сервис вернет результат
     * @param res - Результат сервера
     * @returns {any}
     */
    postResponse(res: Response) {
        return res;
    }

    /**
     * Функция которую если переопредилить будет возвращать обернутые обьекты.
     * @param entity - Модель для генерации
     */
    createEntity(entity: T): T {
        return entity;
    }


    /**
     * Обарачивает данные с помощью функции createEntity для возврящяемыш данных IResultList
     * @param res - Ответ сервера
     * @returns {IResultList<T>}
     */
    protected wrapObjects(res: IResultList<T>) {
        res.data.data = res.data.data.map((entity: T) => {
            return this.createEntity(entity);
        });
        return res;
    }

    /**
     * Обарачивает данные с помощью функции createEntity для возврящяемыш данных IResult
     * @param res - Ответ сервера
     * @returns {IResult<T>}
     */
    protected wrapObject(res: IResult<T>) {
        res.data = this.createEntity(res.data);
        return res;
    }
}
