export interface IPagination {

    page: number;
    total: number;
    last_page: number;
}

export class PaginationModel implements IPagination{
    page: number;
    total: number;
    last_page: number;
    per_page : number;

    constructor(pagination: IPagination = { page: 1, total: 1, last_page: 1}){
        this.setPagination(pagination);
    }

    setPagination(pagination: IPagination){
        this.page         = pagination.page;
        this.total        = pagination.total;
        this.last_page    = pagination.last_page;
    }

    getPagination() {
        return {
            page         : this.page,
            total        : this.total,
            last_page    : this.last_page
        }
    }
}