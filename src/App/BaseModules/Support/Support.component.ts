import {Component, EventEmitter, Output, ViewChild} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {ValidatorService} from "../Validation/Validation.service";
import {OpenService} from "../../Services/OpenService/OpenService.service";
import {AuthorizationService} from "../Authorization/Authorization.service";


@Component({
    selector: 'support',
    templateUrl: './Support.component.html',
    styleUrls : ['./Support.component.less']
})

export class SupportComponent {

    @Output() private onSuccess : EventEmitter<any> = new EventEmitter<any>();
    @Output() private onError : EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('loader') private loader : any;
    @ViewChild('error') private error : any;

    form : FormGroup;

    serverError : string = '';

    constructor(
        public  AuthorizationService : AuthorizationService,
        private ValidatorService : ValidatorService,
        private OpenService : OpenService
    ) {
        this.form = new FormGroup({
            topic    : new FormControl(''),
            message  : new FormControl(''),
            email    : new FormControl({ value : "", disabled : this.AuthorizationService.isAuthenticated }),
            status   : new FormControl('high'),
            priority : new FormControl('high'),
        });
    }

    submit() {
        let data = this.form.value;
        if(this.AuthorizationService.isAuthenticated) {
            data.email = this.AuthorizationService.user.email;
        }
        this.loader.show();
        this.OpenService.support(data).subscribe(() => {
            this.form.reset();
            this.loader.hide();
            this.onSuccess.emit();
        }, err => {
            this.ValidatorService.addErrorToForm(this.form, err);
            this.loader.hide();
            this.onError.emit(err);
        })
    }

}
