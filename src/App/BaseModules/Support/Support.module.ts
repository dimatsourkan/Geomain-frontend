import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import {SupportComponent} from "./Support.component";
import {FormControlsModule} from "../../Components/FormControls/FormControls.module";
import {ModalModule} from "../Modal/Modal.module";
import {LoaderModule} from "../Loader/Loader.module";
import {ValidationModule} from "../Validation/Validation.module";
import {OpenServiceModule} from "../../Services/OpenService/OpenService.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FormControlsModule,
        OpenServiceModule,
        ModalModule,
        LoaderModule,
        ValidationModule,
    ],
    providers: [
    ],
    declarations: [
        SupportComponent
    ],
    exports: [
        SupportComponent
    ]
})

export class SupportModule {}
