import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import {FormControlsModule} from "../../Components/FormControls/FormControls.module";
import {ModalModule} from "../Modal/Modal.module";
import {LoaderModule} from "../Loader/Loader.module";
import {ValidationModule} from "../Validation/Validation.module";
import {SearchComponent} from "./Search.component";
import {OpenServiceModule} from "../../Services/OpenService/OpenService.module";
import {GeoNumberModule} from "../../EntityModules/GeoNumber/GeoNumber.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FormControlsModule,
        ModalModule,
        LoaderModule,
        GeoNumberModule,
        OpenServiceModule,
        ValidationModule,
    ],
    providers: [
    ],
    declarations: [
        SearchComponent
    ],
    exports: [
        SearchComponent
    ]
})

export class SearchModule {}
