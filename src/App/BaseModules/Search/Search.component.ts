import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {GeoNumberService} from "../../EntityModules/GeoNumber/GeoNumber.service";
import {IGeoNumber} from "../../EntityModules/GeoNumber/GeoNumber.model";
import {OpenService} from "../../Services/OpenService/OpenService.service";


@Component({
    selector: 'search',
    templateUrl: './Search.component.html',
    styleUrls : ['./Search.component.less']
})

export class SearchComponent {

    @Input() searchMethod : string;

    @ViewChild('search') private search : any;
    @ViewChild('loader') private loader : any;

    @Output() OnSuccess : EventEmitter<any> = new EventEmitter<any>();

    serverError : boolean = false;

    constructor(
        private GeoNumberService : GeoNumberService,
        private OpenService      : OpenService,
    ) {

    }

    get value() {
        return this.search.nativeElement.value.replace(/-/g, '');
    }

    Search() {
        let Search : any = null;

        switch(this.searchMethod) {
            case 'whoIs'      : Search = this.SearchWhoIs();     break;
            case 'geounumber' : Search = this.SearchGeonumber(); break;
            default           : Search = this.SearchGeonumber(); break;
        }

        this.loader.show();
        Search.subscribe((res : any) => {
            this.loader.hide();
            this.OnSuccess.emit({
                value  : this.value,
                result : res
            });
        }, (err : any) => {
            this.serverError = true;
            this.loader.hide();
        });
    }

    SearchGeonumber() {
        return this.GeoNumberService.find(this.value);
    }

    SearchWhoIs() {
        return this.OpenService.whoIs(this.value);
    }

}
