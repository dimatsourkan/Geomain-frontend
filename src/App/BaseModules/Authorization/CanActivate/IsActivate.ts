import {ActivatedRouteSnapshot, CanActivate} from "@angular/router";
import {AuthorizationService} from "../Authorization.service";
import {Injectable} from "@angular/core";


@Injectable()
export class IsActivate implements CanActivate {

    constructor(
        private AS : AuthorizationService
    ) {

    }

    canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {

        return new Promise((resolve) => {

            if(this.AS.isAuthenticated) {

                if(this.AS.isRemembered) {

                    this.refreshToken().then(() => {
                        return resolve(true);
                    });

                }
                else {

                    return this.AS.getProfile().subscribe(() => {
                        return resolve(true);
                    }, () => resolve(true));

                }
            }
            else {
                return resolve(true);
            }

        });
    }

    refreshToken() : Promise<any> {

        return new Promise((resolve, reject) => {

            this.AS.refreshToken().subscribe(() => {

                return this.AS.getProfile().subscribe(() => {

                    resolve(true);

                }, () => resolve(true));

            }, () => resolve(true));

        });

    }

}