import {ActivatedRouteSnapshot, CanActivate, Router} from "@angular/router";
import {AuthorizationService} from "../Authorization.service";
import {Injectable} from "@angular/core";


@Injectable()
export class IsAuthenticated implements CanActivate {

    constructor(
        private AS : AuthorizationService,
        private Router: Router
    ) {

    }

    canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {

        return new Promise((resolve) => {

            if (this.AS.isAuthenticated) {

                if(this.AS.user) {

                    if(this.AS.fromRoles(route.data['roles'])) {
                        return resolve(true);
                    }
                    else {
                        this.Router.navigate(['']);
                        return resolve(false);
                    }

                }
                else {

                    this.AS.logout();
                    this.Router.navigate(['']);
                    return resolve(false);

                }
            }

            this.Router.navigate(['']);
            return resolve(false);
        });
    }

}