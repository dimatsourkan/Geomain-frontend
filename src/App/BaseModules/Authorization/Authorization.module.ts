import { NgModule } from '@angular/core';
import { AuthorizationService } from './Authorization.service';
import {IsAuthenticated} from "./CanActivate/IsAuthenticated";
import {NotAuthenticated} from "./CanActivate/NotAuthenticated";
import {UserModule} from "../../EntityModules/User/User.module";
import {AppHttpModule} from "../Http/Http.module";
import {IsActivate} from "./CanActivate/IsActivate";

@NgModule({
    imports: [
        AppHttpModule,
        UserModule
    ],
    declarations: [
    ],
    exports : [
    ],
    providers: [
        AuthorizationService,
        IsAuthenticated,
        NotAuthenticated,
        IsActivate
    ]
})

export class AuthorizationModule {}
