import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AUTH_URL, CREDENTIALS } from '../../constants';
import { TokenService } from '../../BaseClasses/Services/Token.service';
import 'rxjs/add/operator/map';
import {IUser} from "../../EntityModules/User/User.model";
import {UserService} from "../../EntityModules/User/Users.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";

let instanse : any = null;

@Injectable()
export class AuthorizationService {

    user : IUser;

    /**
     * Хедеры для отправки на сервер
     */
    headers: HttpHeaders;

    constructor(
        private http: HttpClient,
        private token: TokenService,
        private UserService : UserService
    ) {

        if(instanse) {
            return instanse;
        }

        instanse = this;

        this.headers = new HttpHeaders({
            'Authorization' : `Basic ${btoa(CREDENTIALS)}`
        });
    }

    /**
     * Авторизация пользователя
     * @param {string} username
     * @param {string} password
     * @param {boolean} rememberMe
     * @returns {Observable<>}
     */
    login(username: string, password : string, rememberMe : boolean = false): Observable<any> {

        this.setRememberData(rememberMe);

        return this.http
            .post(`${AUTH_URL}/oauth/token`,{ },
                { headers : this.headers, params : { username, password, grant_type : 'password' } }
                )
            .map<any, any>((res) => this.setLogin(res))
    }

    refreshToken() {
        return this.http
            .post(`${AUTH_URL}/oauth/token`,
                { refresh_token : this.token.getRefreshToken(), grant_type : 'refresh_token' },
                { headers : this.headers }
            )
            .map<any, any>((res) => this.setLogin(res))
    }

    confirm(token: string): Observable<any> {
        return this.http
            .get(`${AUTH_URL}/api/auth/verification/${token}`, { headers : this.headers });
    }

    forgotPass(login: string): Observable<any> {
        return this.http
            .get(`${AUTH_URL}/api/open/user/forgot-password`, { headers : this.headers, params : {login} })
    }

    changePass(password: string, token : string): Observable<any> {
        return this.http
            .post(`${AUTH_URL}/api/open/user/update-password`, {password, token}, { headers : this.headers });
    }

    confirmPass(password: string, token : string): Observable<any> {
        return this.http
            .post(`${AUTH_URL}/api/auth/password-confirm`, {password, token}, { headers : this.headers });
    }

    registrarSignUp(formData: any): Observable<any> {
        return this.http
            .post(`${AUTH_URL}/api/open/user/registrar-signup`, formData, { headers : this.headers });
    }

    setLogin(res: any) {
        /** Установка токена **/
        this.token.setTokens(res.access_token, res.refresh_token);
        /** Возвращаем результат **/
        return res;
    }

    /**
     * Логаут - требуется только вызвать этот метод
     * Токен для идентификации подставляется сам
     * @returns {Observable<Response>}
     */
    logout() {
        this.token.resetToken();
        this.user = null;
        // document.location.reload();
    }

    /**
     * Проверяет авторизован ли пользователь по наличию токена в локальном хранилище
     * @returns {Promise<boolean>}
     */
    get isAuthenticated() : boolean {
        return !!this.token.getToken();
    }

    fromRoles(roles : string[]) {

        if(this.isAuthenticated && this.user) {

            if(roles.indexOf('*') >= 0) {
                return true;
            }

            if(roles.indexOf(this.user.role) >= 0) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }

    }

    getProfile() {
        return this.UserService.getProfile().map((res : IUser) => {
            this.user = res;
            return res;
        });
    }

    setRememberData(rememberMe : boolean = false) {

        localStorage.setItem('refreshToken', `${rememberMe}`);
        localStorage.setItem('refreshDate', `${new Date()}`);
    }

    get isRemembered() {

        let oldDate : Date = new Date(localStorage.getItem('refreshDate'));
        let date    : Date = new Date();

        oldDate.setHours(oldDate.getHours()+3);

        if(localStorage.getItem('refreshToken')) {
            return oldDate.getTime() < date.getTime();
        }
        else {
            return false;
        }

    }
}