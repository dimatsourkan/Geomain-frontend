import { AppHttpService } from './Http.service';
import {HttpHandler} from "@angular/common/http";

export function AppHttpFactory(
    HttpHandler: HttpHandler
) {
    return new AppHttpService(HttpHandler);
}
