import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { TokenService } from '../../BaseClasses/Services/Token.service';
import {HttpClient, HttpErrorResponse, HttpHandler, HttpHeaders} from "@angular/common/http";

@Injectable()
export class AppHttpService extends HttpClient {

    protected token: TokenService;

    constructor(HttpHandler : HttpHandler) {
        super(HttpHandler);

        this.token = new TokenService();
    }

    /**
     * Функция добавляет хередеры перед отправкой запроса
     * @param options
     */
    private modifyHeaders(options?: any) {

        if (options) {

            let headers = {
                // 'X-Requested-With': 'XMLHttpRequest'
            };

            if(options.headers.get('Authorization')) {
                headers['Authorization'] = options.headers.get('Authorization');
            }
            else {
                if(this.token.getToken()) {
                    headers['Authorization'] = `Bearer ${this.token.getToken()}`;
                }
            }

            options.headers = new HttpHeaders(headers);
        }

    }


    get(url: string, options?: any): Observable<any> {

        this.modifyHeaders(options);
        return super.get(url, options).pipe(
            map(res => this.returnRes(res)),
            catchError((e) => this.errorHandler(e))
        );
    }

    post(url: string, body: any, options?: any): Observable<any> {
        this.modifyHeaders(options);
        return super.post(url, body, options).pipe(
            map(res => this.returnRes(res)),
            catchError((e) => this.errorHandler(e))
        );
    }

    put(url: string, body: any, options?: any): Observable<any> {

        this.modifyHeaders(options);
        return super.put(url, body, options).pipe(
            map(res => this.returnRes(res)),
            catchError((e) => this.errorHandler(e))
        );
    }

    delete(url: string, options?: any): Observable<any> {

        this.modifyHeaders(options);
        return super.delete(url, options).pipe(
            map(res => this.returnRes(res)),
            catchError((e) => this.errorHandler(e))
        );
    }

    private returnRes(res : any){
         return res;
    }

    private errorHandler(error: HttpErrorResponse) : any {
        this.catchErrors(error);
        throw(error.error);
    }

    catchErrors(error : any) {
        console.log(error);
        switch (error.status) {
            case 401 : this.token.resetToken();
        }
    }
}
