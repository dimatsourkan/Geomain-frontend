import {NgModule} from '@angular/core';
import { AppHttpFactory } from './Http.facory';
import {HttpClient, HttpHandler} from "@angular/common/http";


@NgModule({
    providers: [
        {
            provide: HttpClient,
            useFactory: AppHttpFactory,
            deps: [ HttpHandler ]
        }
    ],
})
export class AppHttpModule {}
