import {NgModule}       from "@angular/core";
import {CommonModule} from "@angular/common";
import { Modal } from './Components/Modal/Modal.component';
import {ErrorModal} from "./Components/Error/Error.component";
import {ConfirmModal} from "./Components/Confirm/Confirm.component";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        Modal,
        ErrorModal,
        ConfirmModal
    ],
    exports: [
        Modal,
        ErrorModal,
        ConfirmModal
    ]

})
export class ModalModule {}

