import {Component, Input, ViewChild} from '@angular/core';

@Component({
    selector: 'error',
    templateUrl: 'Error.component.html',
    styleUrls: ['Error.component.less']
})

export class ErrorModal {

    @ViewChild('modal') private modal : any;

    @Input() error : string = '';
    @Input() title : string = 'Server Error';

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

}
