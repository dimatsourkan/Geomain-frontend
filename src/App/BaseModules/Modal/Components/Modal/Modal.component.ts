import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';
import * as jQuery from 'jquery';
require('../../../../../../node_modules/remodal/src/remodal.js');

@Component({
    selector: 'modal',
    templateUrl: 'Modal.component.html',
    styleUrls: ['Modal.component.less']
})

export class Modal implements AfterViewInit, OnDestroy {

    @ViewChild('modal') private modal : any;

    @Input('options') private options : any = {};

    @Output('onClose') private onClose : EventEmitter<any> = new EventEmitter<any>();

    private $modal : any;

    open() {
        this.$modal.open();
    }

    close() {
        this.$modal.close();
    }

    ngAfterViewInit() {
        let $el : any = jQuery(this.modal.nativeElement);
        this.$modal = $el.remodal(this.options);
        $(this.$modal.$modal).on('closing', () => {
            this.onClose.emit();
        });
    }

    ngOnDestroy() {
        this.$modal.destroy();
    }

}
