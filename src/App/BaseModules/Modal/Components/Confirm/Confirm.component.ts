import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';

@Component({
    selector: 'confirm',
    templateUrl: 'Confirm.component.html',
    styleUrls: ['Confirm.component.less']
})

export class ConfirmModal {

    @ViewChild('modal') private modal : any;

    @Output('onSuccess') onSuccess : EventEmitter<any> = new EventEmitter<any>()

    @Input() error : string = '';

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

}
