import {NgModule} from '@angular/core';
import {PaginationComponent} from "./Pagination.component";
import {CommonModule} from "@angular/common";
import {PaginationService} from "./Pagination.service";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        PaginationComponent
    ],
    exports: [
        PaginationComponent
    ],
    providers : [
        PaginationService
    ]
})
export class PaginationModule{}
