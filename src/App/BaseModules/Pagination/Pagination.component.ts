import {Component, Input, EventEmitter, Output, OnChanges} from '@angular/core';
import {PaginationService} from "./Pagination.service";

/**
 * Описание компоненты пагинации
 */
@Component({
    selector: 'pagination',
    templateUrl: './Pagination.component.html',
    styleUrls: ['./Pagination.component.less']
})


/**
 * Компонента пагинации
 */
export class PaginationComponent implements OnChanges {

    @Input() current : number;
    @Input() total   : number;
    @Input() showed  : number = 5;

    @Output() onChangePage = new EventEmitter<number>();
    @Output() showMore      = new EventEmitter<number>();

    showedPages : number[];

    constructor(private paginationService : PaginationService) {}

    /**
     * Указывает на то что в пагинации содержится только одна страница
     * @returns {boolean}
     */
    public hasOnePage() {
        if(this.total <= 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Указывает на то что это первая страница
     * @returns {boolean}
     */
    public isFirstPage() {
        if(this.current == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Указывает на то что это последняя страница
     * @returns {boolean}
     */
    public isLastPage() {
        if(this.current == this.total) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Клик по странице в пагинации
     * @param page - Номер страницы которую нужно грузить
     */
    changePage(page : number) {
        this.onChangePage.emit(page);
    }

    ngOnChanges() {
        this.showedPages = this.paginationService.getPaginationPages(this.current, this.total, this.showed);
    }
}
