import {IPagination} from "../../BaseClasses/Models/Pagination.model";

export class PaginationModel implements IPagination{
    page: number;
    total: number;
    last_page: number;

    constructor(pagination: IPagination = { page: 1, total: null, last_page: null }){
        this.setPagination(pagination);
    }

    setPagination(pagination: IPagination){
        this.page         = pagination.page;
        this.total        = pagination.total;
        this.last_page    = pagination.last_page;
    }
}
