import {Injectable} from "@angular/core";
import {FormGroup} from "@angular/forms";


@Injectable()
export class ValidatorService {

    clearValidation(form : FormGroup) {
        for(let key in form.controls) {
            form.controls[key].setErrors(null);
        }
    }

    addErrorToForm(form: FormGroup, errors: any){

        if(!errors.fieldErrors) return ;

        if(!form.controls) {
            return;
        }

        this.clearValidation(form);

        console.log(errors);

        errors.fieldErrors.map((err : any) => {
            if(form.controls[err.field]) {
                form.controls[err.field].setErrors({ server : err.message });
            }
        })

    }

}
