import {GeomainInfoPageComponent} from "./Components/Page/Page.component";
import {RouterModule} from "@angular/router";

export const routes = [
    {
        path: '',
        component : GeomainInfoPageComponent
    },
];

export const GEOMAIN_INFO_ROUTING = RouterModule.forChild(routes);