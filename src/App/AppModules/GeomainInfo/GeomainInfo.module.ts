import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {GeomainInfoPageComponent} from "./Components/Page/Page.component";
import {GEOMAIN_INFO_ROUTING} from "./GeomainInfo.routing";
import {AccordionModule} from "ngx-accordion";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        LoaderModule,
        AccordionModule,

        GEOMAIN_INFO_ROUTING
    ],
    declarations: [
        GeomainInfoPageComponent
    ],
    exports : [
    ],
    providers: []
})

export class GeomainInfoModule {

}
