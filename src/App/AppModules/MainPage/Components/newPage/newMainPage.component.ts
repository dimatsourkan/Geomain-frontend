import { Component, ElementRef, ViewChild } from '@angular/core';
import {Helpers} from "../../../../BaseClasses/Helpers";
import {Router} from "@angular/router";
import { GeoNumberService } from '../../../../EntityModules/GeoNumber/GeoNumber.service';
import { USER_ROLE } from '../../../../EntityModules/User/User.model';
import { AuthorizationService } from '../../../../BaseModules/Authorization/Authorization.service';


@Component({
    selector: 'main-page',
    templateUrl: './newMainPage.component.html',
    styleUrls : ['./newMainPage.component.less']
})

export class newMainPageComponent {

    Helpers : Helpers = new Helpers();

    @ViewChild('search') private search : any;
    @ViewChild('loader') private loader : any;

    serverError : boolean = false;

    nawShowed : boolean = false;

    USER_ROLE : any = USER_ROLE;

    options = {
        slidesToShow : 1,
        arrows : false,
        pauseOnHover : false,
        autoplay : true,
        autoplaySpeed : 5000,
        dots : false,
        customPaging: function() {
            return $('<button type="button" />');
        },
    };

    constructor(
        private Router : Router,
        public AuthorizationService : AuthorizationService,
        private GeoNumberService : GeoNumberService,
    ) {

    }

    get value() {
        return this.search.nativeElement.value.replace(/-/g, '');
    }

    runSearch() {

        this.loader.show();
        this.GeoNumberService.find(this.value).subscribe((res: any) => {
            this.loader.hide();
            this.Router.navigate(['/', this.value]);
        }, (err: any) => {
            console.log(err);
            this.serverError = true;
            this.loader.hide();
        })
    }

    logout() {
        this.AuthorizationService.logout();
        this.Router.navigate(['/']);
    }


}
