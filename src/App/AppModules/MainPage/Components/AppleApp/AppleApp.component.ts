import { Component} from "@angular/core";


@Component({
    selector: 'apple-app',
    template: ``
})

export class AppleAppComponent {
    constructor() {
        document.location.href = 'https://itunes.apple.com/ua/app/geomain-the-nextgen-address/id1375477836?mt=8';
    }
}
