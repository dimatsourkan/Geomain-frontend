import { Component, ViewEncapsulation} from "@angular/core";
import {Helpers} from "../../../../BaseClasses/Helpers";
import {IGeoNumber} from "../../../../EntityModules/GeoNumber/GeoNumber.model";
import {Router} from "@angular/router";


@Component({
    selector: 'main-page',
    templateUrl: './MainPage.component.html',
    styleUrls : ['./MainPage.component.less'],
    encapsulation : ViewEncapsulation.None
})

export class MainPageComponent {

    Helpers : Helpers = new Helpers();

    slickOptions : any = {
        arrows : false,
        dots : true,
        autoplay : true,
        autoplaySpeed : 10000,
        customPaging: (slider : any) => {
            return $('<button type="button" />');
        }
    };

    constructor(private Router : Router) {

    }

    onFind(result : { value : string, result : IGeoNumber }) {
        this.Router.navigate(['/', result.value]);
    }

}
