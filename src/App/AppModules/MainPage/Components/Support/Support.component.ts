import {Component, ViewChild} from "@angular/core";
import {Router} from "@angular/router";


@Component({
    selector: 'support-page',
    templateUrl: './Support.component.html',
    styleUrls : ['./Support.component.less']
})

export class SupportPageComponent {

    @ViewChild('modal') private modal : any;

    constructor(private Router : Router) {}

    modalOptions : any = {
        closeOnConfirm : false,
        closeOnCancel : false,
        closeOnEscape : false,
        closeOnOutsideClick : false
    };

    closeModal() {
        this.modal.close();
        $('body').css('padding-right', 0);
        this.Router.navigate(['/']);
    }
}
