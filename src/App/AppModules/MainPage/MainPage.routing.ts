import {MainPageComponent} from "./Components/Page/MainPage.component";
import {SupportPageComponent} from "./Components/Support/Support.component";
import { newMainPageComponent } from './Components/newPage/newMainPage.component';
import { AppleAppComponent } from './Components/AppleApp/AppleApp.component';

export const MAIN_PAGE_ROUTING = [
    {
        path: '',
        component : newMainPageComponent
    },

    {
        path: 'home',
        component : newMainPageComponent
    },
    {
        path: 'appleapp',
        component : AppleAppComponent
    },
    {
        path: 'home/old',
        component : MainPageComponent
    },
    {
        path: 'support',
        component : SupportPageComponent
    }
];