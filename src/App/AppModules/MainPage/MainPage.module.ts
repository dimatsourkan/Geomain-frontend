import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {TranslateModule} from "@ngx-translate/core";
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {SlickModule} from "../../Directives/Slick/Slick.module";
import {AuthorizationModule} from "../../BaseModules/Authorization/Authorization.module";
import {GeoNumberModule} from "../../EntityModules/GeoNumber/GeoNumber.module";
import {SupportPageComponent} from "./Components/Support/Support.component";
import {FormControlsModule} from "../../Components/FormControls/FormControls.module";
import {SupportModule} from "../../BaseModules/Support/Support.module";
import {ModalModule} from "../../BaseModules/Modal/Modal.module";
import {SearchModule} from "../../BaseModules/Search/Search.module";
import { HeaderModule } from '../../Components/Header/Header.module';
import { FooterModule } from '../../Components/Footer/Footer.module';
import { newMainPageComponent } from './Components/newPage/newMainPage.component';
import { DropdownModule } from 'ngx-dropdown';
import {MainPageComponent} from "./Components/Page/MainPage.component";
import { AppleAppComponent } from './Components/AppleApp/AppleApp.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        LoaderModule,

        DropdownModule,
        FooterModule,
        HeaderModule,
        SlickModule,
        SearchModule,
        FormControlsModule,
        AuthorizationModule,
        GeoNumberModule,
        SupportModule,
        ModalModule
    ],
    declarations: [
        SupportPageComponent,
        MainPageComponent,
        newMainPageComponent,
        AppleAppComponent
    ],
    exports : [
        SupportPageComponent,
        MainPageComponent,
        newMainPageComponent,
        AppleAppComponent
    ],
    providers: []
})

export class MainPageModule {}
