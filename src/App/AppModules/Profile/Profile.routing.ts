import {IsAuthenticated} from "../../BaseModules/Authorization/CanActivate/IsAuthenticated";
import {ProfilePageComponent} from "./Components/Page/Page.component";
import {RouterModule} from "@angular/router";
import {USER_ROLE} from "../../EntityModules/User/User.model";

export const routes = [
    {
        path: '',
        component : ProfilePageComponent,
        canActivate : [ IsAuthenticated ],
        data : {
            roles : [ USER_ROLE.ROLE_PERSONAL ]
        }
    },
];

export const PROFILE_ROUTING = RouterModule.forChild(routes);