import {Component, ViewChild} from "@angular/core";
import {Helpers} from "../../../../BaseClasses/Helpers";
import {AuthorizationService} from "../../../../BaseModules/Authorization/Authorization.service";
import {FormControl, FormGroup} from "@angular/forms";
import {IUser, User} from "../../../../EntityModules/User/User.model";
import {UserService} from "../../../../EntityModules/User/Users.service";
import {IGeoNumber, GEONUMBER_CATEGORY} from "../../../../EntityModules/GeoNumber/GeoNumber.model";
import {PersonalService} from "../../../../Services/PersonalService/PersonalService.service";


@Component({
    selector: 'profile-page',
    templateUrl: './Page.component.html',
    styleUrls : ['./Page.component.less']
})

export class ProfilePageComponent {

    @ViewChild('errorModal') private errorModal : any;
    @ViewChild('fileForm') private fileForm : any;
    @ViewChild('loader') private loader : any;

    serverError : string = null;

    Helpers : Helpers = new Helpers();

    form : FormGroup;

    // geonumbers : IGeoNumber[] = [];
    // geonames   : IGeoNumber[] = [];
    geomain    : IGeoNumber;

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    constructor(
        public AuthorizationService : AuthorizationService,
        public PersonalService : PersonalService,
        public UserService : UserService
    ) {

        this.form = new FormGroup({
            firstName : new FormControl(this.AuthorizationService.user.firstName),
            lastName  : new FormControl(this.AuthorizationService.user.lastName),
            email     : new FormControl(this.AuthorizationService.user.email),
            password  : new FormControl(null),
            newPass   : new FormControl(null),
        });
    }

    get updatedData() {
        let user : IUser   = new User(this.AuthorizationService.user);
            user.firstName = this.form.value.firstName;
            user.lastName  = this.form.value.lastName;
        return user;
    }

    uploadAvatar(form : any) {
        this.loader.show();
        this.UserService.uploadAvatar(form).subscribe(() => {
            this.AuthorizationService.getProfile().subscribe();
            this.loader.hide();
        }, err => {
            this.serverError = err.message;
            this.errorModal.open();
            this.loader.hide()
        });
    }

    removeAvatar() {
        this.loader.show();
        this.UserService.deleteAvatar().subscribe(res => {
            this.AuthorizationService.getProfile().subscribe();
            this.loader.hide();
        },(err : any) => {
            this.serverError = err.message;
            this.errorModal.open();
            this.loader.hide();
        });
    }

    updateProfile() {
        this.loader.show();
        this.PersonalService.updateProfile(this.updatedData).subscribe(user => {
            this.AuthorizationService.getProfile().subscribe();
            this.loader.hide();
        }, (err : any) => {
            this.serverError = err.message;
            this.errorModal.open();
            this.loader.hide();
        });
    }

    changePass() {
        if(!this.form.value.password || !this.form.value.newPass) return;

        this.loader.show();
        this.PersonalService.changePass(this.form.value.password, this.form.value.newPass).subscribe(user => {
            this.loader.hide();
            this.form.patchValue({
                password : null,
                newPass : null
            })
        }, (err : any) => {
            this.serverError = err.message;
            this.errorModal.open();
            this.loader.hide();
        });
    }

    update() {
        this.updateProfile();
        this.changePass();
    }

}
