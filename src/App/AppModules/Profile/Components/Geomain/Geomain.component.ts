import {Component, ViewChild} from "@angular/core";
import {Helpers} from "../../../../BaseClasses/Helpers";
import {AuthorizationService} from "../../../../BaseModules/Authorization/Authorization.service";
import {FormControl, FormGroup} from "@angular/forms";
import {UserService} from "../../../../EntityModules/User/Users.service";
import {IGeoNumber, GEONUMBER_CATEGORY} from "../../../../EntityModules/GeoNumber/GeoNumber.model";
import {PersonalService} from "../../../../Services/PersonalService/PersonalService.service";
import {ValidatorService} from "../../../../BaseModules/Validation/Validation.service";


@Component({
    selector: 'profile-geomain',
    templateUrl: './Geomain.component.html',
    styleUrls : ['./Geomain.component.less']
})

export class ProfileGeomainComponent {

    @ViewChild('geomainInputRadio') private geomainInputRadio : any;
    @ViewChild('selectError') private selectError : any;
    @ViewChild('errorModal') private errorModal : any;
    @ViewChild('confirm') private confirm : any;
    @ViewChild('loader') private loader : any;
    @ViewChild('modal') private modal : any;

    serverError : string = null;

    Helpers : Helpers = new Helpers();

    form : FormGroup;

    geonumbers : IGeoNumber[] = [];
    geonames   : IGeoNumber[] = [];
    geomain    : IGeoNumber;

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    constructor(
        private ValidatorService : ValidatorService,
        public AuthorizationService : AuthorizationService,
        public UserService : UserService
    ) {
        this.getGeomains();
        this.form = new FormGroup({
            geonumber : new FormControl('')
        })
    }

    getGeomains() {
        this.UserService.getGeomains().subscribe(res => {
            this.geonames = res.filter((item : IGeoNumber) => {
                if(item.name) return true;
                else return false
            });

            this.geonumbers = res.filter((item : IGeoNumber) => {
                if(!item.name) return true;
                else return false
            });
        })
    }

    toRemove(geomain : IGeoNumber) {
        this.geomain = geomain;
        this.confirm.open();
    }

    removeGeomain() {
        this.UserService.deleteGeomain(this.geomain).subscribe(() => {
            this.getGeomains();
            this.confirm.close();
        },(err : any) => {
            this.confirm.close();
            this.serverError = err.message;
            this.errorModal.open();
            this.loader.hide();
        })
    }

    changeCheck(geomain : IGeoNumber, geonum : boolean = false) {

        if(geonum) {
            this.uncheckGeomains();
        }

        geomain.checked = !geomain.checked;
    }

    checkInput() {
        this.uncheckGeomains();
        this.geomainInputRadio.checked = true;
        this.geomainInputRadio.input.nativeElement.checked = true;
    }

    uncheckGeomains() {
        this.geonumbers.map((item) => {
            item.checked = false;
        });
    }

    get inputChecked() {
        return this.geomainInputRadio.input.nativeElement.checked;
    }

    linkGeomains() {

        let number : any = this.form.value.geonumber.replace(/\D+/g,"");

        if(this.inputChecked && !number) {
            return this.selectError.open();
        }
        else if(!this.inputChecked) {
            number = this.geonumbers.filter((item) => item.checked).map(item => item.id)[0];
        }

        let names : any[] = this.geonames.filter((item) => {
            return item.checked;
        }).map(item => item.id);

        if(!number || !names.length) {
            return this.selectError.open();
        }

        this.link(names, number);
    }

    getlinkMethod(names : any[], number : any) : any {
        if(this.inputChecked) {
            return this.UserService.linkByNumber(names, number);
        }
        else {
            return this.UserService.linkById(names, number);
        }
    }

    link(names : any[], number : any) {
        this.loader.show();
        this.getlinkMethod(names, number).subscribe((res : any) => {
            this.loader.hide();
            this.getGeomains();
            this.modal.open();
        }, (err : any) => {
            this.ValidatorService.addErrorToForm(this.form, err);
            this.loader.hide();
            if(err.message) {
                this.serverError = err.message;
                this.errorModal.open();
            }
        })
    }

}
