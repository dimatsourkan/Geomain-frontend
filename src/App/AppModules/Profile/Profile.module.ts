import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {TranslateModule} from "@ngx-translate/core";
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {AuthorizationModule} from "../../BaseModules/Authorization/Authorization.module";
import {GeoNumberModule} from "../../EntityModules/GeoNumber/GeoNumber.module";
import {ProfilePageComponent} from "./Components/Page/Page.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserModule} from "../../EntityModules/User/User.module";
import {ModalModule} from "../../BaseModules/Modal/Modal.module";
import {PROFILE_ROUTING} from "./Profile.routing";
import {FormControlsModule} from "../../Components/FormControls/FormControls.module";
import {PersonalServiceModule} from "../../Services/PersonalService/PersonalService.module";
import {ProfileGeomainComponent} from "./Components/Geomain/Geomain.component";
import {ValidationModule} from "../../BaseModules/Validation/Validation.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        TranslateModule,
        LoaderModule,
        ValidationModule,
        FormControlsModule,

        AuthorizationModule,
        UserModule,
        ModalModule,
        GeoNumberModule,
        PersonalServiceModule,

        PROFILE_ROUTING
    ],
    declarations: [
        ProfileGeomainComponent,
        ProfilePageComponent
    ],
    exports : [
    ],
    providers: []
})

export class ProfileModule {

}
