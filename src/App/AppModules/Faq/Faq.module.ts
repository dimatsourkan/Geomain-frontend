import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {FaqPageComponent} from "./Components/Page/Page.component";
import {PROFILE_ROUTING} from "./Faq.routing";
import {AccordionModule} from "ngx-accordion";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        LoaderModule,
        AccordionModule,

        PROFILE_ROUTING
    ],
    declarations: [
        FaqPageComponent
    ],
    exports : [
    ],
    providers: []
})

export class FaqModule {

}
