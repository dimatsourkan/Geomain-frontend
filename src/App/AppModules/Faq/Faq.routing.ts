import {FaqPageComponent} from "./Components/Page/Page.component";
import {RouterModule} from "@angular/router";

export const routes = [
    {
        path: '',
        component : FaqPageComponent
    },
];

export const PROFILE_ROUTING = RouterModule.forChild(routes);