import {BusinessSolPageComponent} from "./Components/Page/Page.component";
import {RouterModule} from "@angular/router";

export const routes = [
    {
        path: '',
        component : BusinessSolPageComponent
    },
];

export const BUSINESS_SOL_ROUTING = RouterModule.forChild(routes);