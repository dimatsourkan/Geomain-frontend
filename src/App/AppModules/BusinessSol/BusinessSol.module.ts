import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {BusinessSolPageComponent} from "./Components/Page/Page.component";
import {BUSINESS_SOL_ROUTING} from "./BusinessSol.routing";
import {AccordionModule} from "ngx-accordion";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        LoaderModule,
        AccordionModule,

        BUSINESS_SOL_ROUTING
    ],
    declarations: [
        BusinessSolPageComponent
    ],
    exports : [
    ],
    providers: []
})

export class BusinessSolModule {

}
