import {RegisterComponent} from "./Components/Pages/Register/Register.component";
import {WrapperComponent} from "../../Components/Wrapper/wrapper.component";
import {GeomainMapComponent} from "./Components/Pages/Map/Map.component";
import {GeomainWrapperComponent} from "./Components/Wrapper/Wrapper.component";
import {GeomainQrPageComponent} from "./Components/Pages/QrCode/QrCode.component";
import {GeomainCoordinatesComponent} from "./Components/Pages/Coordinates/Coordinates.component";
import {GeomainGeomarketComponent} from "./Components/Pages/Geomarket/Geomarket.component";
import {GeomainNavigateComponent} from "./Components/Pages/Navigate/Navigate.component";
import {GeomainShareComponent} from "./Components/Pages/Share/Share.component";
import {RouterModule} from "@angular/router";
import {GeomainSequrityPageComponent} from "./Components/Pages/Sequrity/Sequrity.component";
import {GeomainMainPageComponent} from "./Components/Pages/MainPage/MainPage.component";
import {IsAuthenticated} from "../../BaseModules/Authorization/CanActivate/IsAuthenticated";
import {UploadVideoPageComponent} from "./Components/Pages/UploadVideo/UploadVideo.component";
import {UploadBunnerPageComponent} from "./Components/Pages/UploadBunner/UploadBunner.component";
import { GeomainUrlComponent } from './Components/Pages/Url/Url.component';

export const routes = [

    // {
    //     path: 'register',
    //     component : RegisterComponent
    // },

    {
        path: ':number',
        component : GeomainWrapperComponent,
        children : [
            {
                path: 'home',
                component : GeomainMainPageComponent
            },
            {
                path: 'upload-video',
                canActivate : [ IsAuthenticated ],
                component : UploadVideoPageComponent,
                data : { roles : [ '*' ] }
            },
            {
                path: 'upload-bunner',
                canActivate : [ IsAuthenticated ],
                component : UploadBunnerPageComponent,
                data : { roles : [ '*' ] }
            },
            {
                path: 'map',
                component : GeomainMapComponent
            },
            {
                path: 'qr',
                component : GeomainQrPageComponent
            },
            {
                path: 'coordinates',
                canActivate : [ IsAuthenticated ],
                component : GeomainCoordinatesComponent,
                data : { roles : [ '*' ] }
            },
            {
                path: 'geomarket',
                canActivate : [ IsAuthenticated ],
                component : GeomainGeomarketComponent,
                data : { roles : [ '*' ] }
            },
            {
                path: 'navigate',
                component : GeomainNavigateComponent
            },
            {
                path: 'security',
                canActivate : [ IsAuthenticated ],
                component : GeomainSequrityPageComponent,
                data : { roles : [ '*' ] }
            },
            {
                path: 'share',
                component : GeomainShareComponent
            },
            {
                path: 'url',
                component : GeomainUrlComponent
            },
            {
                path: '**',
                redirectTo: 'home'
            }
        ]
    }
];

export const GEOMAIN_ROUTES = RouterModule.forChild(routes);
