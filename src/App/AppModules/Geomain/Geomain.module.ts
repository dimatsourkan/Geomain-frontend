import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {TranslateModule} from "@ngx-translate/core";
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {FooterModule} from "../../Components/Footer/Footer.module";
import {HeaderModule} from "../../Components/Header/Header.module";
import {RegisterComponent} from "./Components/Pages/Register/Register.component";
import {AgmCoreModule} from "@agm/core";
import {MAP_API_KEY} from "../../constants";
import {FormControlsModule} from "../../Components/FormControls/FormControls.module";
import {GeoNumberModule} from "../../EntityModules/GeoNumber/GeoNumber.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ModalModule} from "../../BaseModules/Modal/Modal.module";
import {GeomainMapComponent} from "./Components/Pages/Map/Map.component";
import {WrapperModule} from "../../Components/Wrapper/Wrapper.module";
import {GeomainNameComponent} from "./Components/Head/Components/Name/Name.component";
import {GeomainLinksComponent} from "./Components/Head/Components/Links/Links.component";
import {GeomainDropComponent} from "./Components/Head/Components/Drop/Drop.component";
import {GeomainTimelineComponent} from "./Components/Timeline/Timeline.component";
import {GeomainSocialFeedComponent} from "./Components/SocialFeed/SocialFeed.component";
import {GeopageHeadComponent} from "./Components/Head/Head.component";
import {GeomainWrapperComponent} from "./Components/Wrapper/Wrapper.component";
import {GeomainService} from "./Components/Geomain.service";
import {GeomainQrPageComponent} from "./Components/Pages/QrCode/QrCode.component";
import {GeomainCoordinatesComponent} from "./Components/Pages/Coordinates/Coordinates.component";
import {GeomainGeomarketComponent} from "./Components/Pages/Geomarket/Geomarket.component";
import {GeomainNavigateComponent} from "./Components/Pages/Navigate/Navigate.component";
import {GeomainShareComponent} from "./Components/Pages/Share/Share.component";
import {ClipboardModule} from "../../Directives/Clipboard/Clipboard.module";
import {ShareButtonsModule} from "ngx-sharebuttons";
import {GEOMAIN_ROUTES} from "./Geomain.routing";
import {AppHttpModule} from "../../BaseModules/Http/Http.module";
import {ValidationModule} from "../../BaseModules/Validation/Validation.module";
import {AutoSizeModule} from "../../Directives/Autosize/Autosize.module";
import {PersonalServiceModule} from "../../Services/PersonalService/PersonalService.module";
import {DropdownModule} from "ngx-dropdown";
import {GeomainSequrityPageComponent} from "./Components/Pages/Sequrity/Sequrity.component";
import {GeomainSecNameComponent} from "./Components/Head/Components/SecName/SecName.component";
import {GeomainMainPageComponent} from "./Components/Pages/MainPage/MainPage.component";
import {UploadVideoPageComponent} from "./Components/Pages/UploadVideo/UploadVideo.component";
import {AdServiceModule} from "../../Services/AdService/AdService.module";
import {UploadBunnerPageComponent} from "./Components/Pages/UploadBunner/UploadBunner.component";
import { GeomainUrlComponent } from './Components/Pages/Url/Url.component';
import { MaskModule } from '../../Directives/Mask/Mask.module';
import { UserModule } from '../../EntityModules/User/User.module';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        RouterModule,
        TranslateModule,
        LoaderModule,
        ModalModule,
        ClipboardModule,
        GeoNumberModule,
        FormControlsModule,
        ValidationModule,
        AppHttpModule,
        AutoSizeModule,
        DropdownModule,
        AdServiceModule,
        PersonalServiceModule,
        MaskModule,
        UserModule,
        ShareButtonsModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: MAP_API_KEY
        }),


        GEOMAIN_ROUTES

    ],
    declarations: [
        RegisterComponent,
        UploadVideoPageComponent,
        GeomainMapComponent,
        GeomainUrlComponent,
        GeomainNameComponent,
        GeomainDropComponent,
        GeopageHeadComponent,
        GeomainLinksComponent,
        GeomainShareComponent,
        GeomainQrPageComponent,
        GeomainSecNameComponent,
        GeomainWrapperComponent,
        GeomainMainPageComponent,
        GeomainTimelineComponent,
        GeomainNavigateComponent,
        GeomainGeomarketComponent,
        UploadBunnerPageComponent,
        GeomainSocialFeedComponent,
        GeomainCoordinatesComponent,
        GeomainSequrityPageComponent
    ],
    exports : [

    ],
    providers: [
        GeomainService
    ]
})

export class GeomainModule {

}
