import {Component} from "@angular/core";
import {GeomainService} from "../Geomain.service";


@Component({
    selector: 'geomain-social-feed',
    templateUrl: './SocialFeed.component.html',
    styleUrls : ['./SocialFeed.component.less'],
})

export class GeomainSocialFeedComponent {

    constructor(public GeomainService : GeomainService) {

    }

}
