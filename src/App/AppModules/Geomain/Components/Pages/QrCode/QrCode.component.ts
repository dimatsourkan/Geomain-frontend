import {Component} from "@angular/core";
import {GeomainService} from "../../Geomain.service";


@Component({
    selector: 'geomain-qr',
    templateUrl: './QrCode.component.html',
    styleUrls : ['./QrCode.component.less'],
})

export class GeomainQrPageComponent {

    constructor(public GeomainService : GeomainService) {}
}
