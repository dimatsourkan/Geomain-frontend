import {Component} from "@angular/core";
import {GeomainService} from "../../Geomain.service";


@Component({
    selector: 'geomain-geomarket',
    templateUrl: './Geomarket.component.html',
    styleUrls : ['./Geomarket.component.less'],
})

export class GeomainGeomarketComponent {

    constructor(public GeomainService : GeomainService) {}
}
