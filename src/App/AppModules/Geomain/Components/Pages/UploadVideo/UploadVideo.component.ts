import {Component, OnInit, ViewChild} from "@angular/core";
import {GeomainService} from "../../Geomain.service";
import {AdService} from "../../../../../Services/AdService/AdService.service";


@Component({
    selector: 'geomain-upload-video',
    templateUrl: './UploadVideo.component.html',
    styleUrls : ['./UploadVideo.component.less'],
})

export class UploadVideoPageComponent implements OnInit {

    videos : any[];

    @ViewChild('error') private error : any;
    @ViewChild('loader') private loader : any;
    @ViewChild('confirm') private confirm : any;

    serverError : string;

    selected : any;

    constructor(
        public GeomainService : GeomainService,
        public AdService : AdService
    ) {}

    ngOnInit() {
        this.getFetch();
    }

    getFetch() {
        this.loader.show();
        this.AdService.getAllByBnameId(this.GeomainService.geomain.id).subscribe((res: any[]) => {
            this.videos = res.filter((item : any) => {
                if(item.type == 'VEMB') return item;
            });
            this.loader.hide();
        }, err => {
            this.loader.hide();
        });
    }

    selectVideo(video : any) {
        this.selected = video;
        this.confirm.open();
    }

    uploadVideo(form : any) {
        this.loader.show();
        this.AdService.addBunner(form).subscribe(res => {
            this.getFetch();
        }, err => {
            this.serverError = err.message;
            this.loader.hide();
            this.error.open();
        })
    }

    removeVideo() {
        this.loader.show();
        this.confirm.close();
        this.AdService.deleteAdById(this.selected.id).subscribe(res => {
            this.getFetch();
        }, (err) => {
            this.serverError = err.message;
            this.loader.hide();
            this.error.open();
        });
    }

}
