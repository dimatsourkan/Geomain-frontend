import {Component, OnInit, ViewChild} from "@angular/core";
import {GeomainService} from "../../Geomain.service";


@Component({
    selector: 'geomain-share',
    templateUrl: './Share.component.html',
    styleUrls : ['./Share.component.less'],
})

export class GeomainShareComponent implements OnInit {

    @ViewChild('copyMessage') private copyMessage : any;
    @ViewChild('toCopy') private toCopy : any;

    Url : string = window.location.href;
    Host : string = window.location.host;
    description : string = '';

    constructor(public GeomainService : GeomainService) {}

    showModal() {
        this.copyMessage.open();
        setTimeout(() => {
            this.copyMessage.close();
        }, 500);
    }

    ngOnInit() {
        this.description = this.toCopy.nativeElement.innerText;
    }
}
