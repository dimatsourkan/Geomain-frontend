import {AfterViewInit, Component} from "@angular/core";
import {Helpers} from "../../../../../BaseClasses/Helpers";
import {GeomainService} from "../../Geomain.service";
import {AuthorizationService} from "../../../../../BaseModules/Authorization/Authorization.service";
import {ActivatedRoute} from "@angular/router";
import {AdService} from "../../../../../Services/AdService/AdService.service";
import {GEONUMBER_CATEGORY} from "../../../../../EntityModules/GeoNumber/GeoNumber.model";


@Component({
    selector: 'geomain-page',
    templateUrl: './MainPage.component.html',
    styleUrls : ['./MainPage.component.less'],
})

export class GeomainMainPageComponent implements AfterViewInit {

    Helpers : Helpers = new Helpers();

    geomain : any;

    ad : any;

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    constructor(
        public GeomainService : GeomainService,
        public AS : AuthorizationService,
        private ActivatedRoute : ActivatedRoute,
        public AdService : AdService
    ) {
        this.geomain = this.ActivatedRoute.parent.snapshot.params['number'];
        this.getFetch();
    }

    getFetch() {
        if(this.GeomainService.geomain.name) {
            this.AdService.fetchAdsByGeoname(this.GeomainService.geomain.name).subscribe(res => {
                this.ad = res;
                console.log(this.ad);
            });
        }
    }

    ngAfterViewInit() {
        // let script = document.createElement('script');
        // script.src = '//go.oclasrv.com/apu.php?zoneid=1481454';
        //
        // this.script.nativeElement.appendChild(script);
    }
}
