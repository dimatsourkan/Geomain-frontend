import {Component, OnInit, ViewChild} from "@angular/core";
import {GeomainService} from "../../Geomain.service";
import {AdService} from "../../../../../Services/AdService/AdService.service";


@Component({
    selector: 'geomain-upload-bunner',
    templateUrl: './UploadBunner.component.html',
    styleUrls : ['./UploadBunner.component.less'],
})

export class UploadBunnerPageComponent implements OnInit {

    bunners : any[];

    @ViewChild('error') private error : any;
    @ViewChild('loader') private loader : any;
    @ViewChild('confirm') private confirm : any;

    serverError : string;

    selected : any;

    constructor(
        public GeomainService : GeomainService,
        public AdService : AdService
    ) {}

    ngOnInit() {
        this.getFetch();
    }

    getFetch() {
        this.loader.show();
        this.AdService.getAllByBnameId(this.GeomainService.geomain.id).subscribe((res: any[]) => {
            this.bunners = res.filter((item : any) => {
                if(item.type == 'BANR') return item;
            });
            this.loader.hide();
        }, err => {
            this.loader.hide();
        });
    }

    selectBunner(video : any) {
        this.selected = video;
        this.confirm.open();
    }

    uploadBunner(form : any) {
        this.loader.show();
        this.AdService.addBunner(form).subscribe(res => {
            this.getFetch();
        }, err => {
            this.serverError = err.message;
            this.loader.hide();
            this.error.open();
        })
    }

    removeBunner() {
        this.loader.show();
        this.confirm.close();
        this.AdService.deleteAdById(this.selected.id).subscribe(res => {
            this.getFetch();
        }, (err) => {
            this.serverError = err.message;
            this.loader.hide();
            this.error.open();
        });
    }

}
