import {Component, Input, ViewChild} from "@angular/core";
import {IGeoNumber} from "../../../../../EntityModules/GeoNumber/GeoNumber.model";
import {GeomainService} from "../../Geomain.service";
import {UserService} from "../../../../../EntityModules/User/Users.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthorizationService} from "../../../../../BaseModules/Authorization/Authorization.service";


@Component({
    selector: 'geomain-sequrity',
    templateUrl: './Sequrity.component.html',
    styleUrls : ['./Sequrity.component.less'],
})

export class GeomainSequrityPageComponent {


    @ViewChild('modalEmail') private modalEmail : any;
    @ViewChild('modalPin') private modalPin : any;
    @ViewChild('error') private error : any;
    @ViewChild('loader') private loader : any;

    method : string = 'DISABLED';

    pinForm   : FormGroup;
    emailForm : FormGroup;

    serverError : string = '';
    constructor(
        public GeomainService : GeomainService,
        private UserService : UserService,
        public Router : Router,
        public AS : AuthorizationService
    ) {

        if(!AS.user) {
            this.Router.navigate(['/']);
        }
        else {
            if(AS.user.id != this.GeomainService.geomain.owner.id) {
                this.Router.navigate(['/']);
            }
        }

        this.method = this.GeomainService.geomain.security;

        this.pinForm = new FormGroup({
            one   : new FormControl(''),
            two   : new FormControl(''),
            three : new FormControl(''),
            four  : new FormControl('')
        });

        this.emailForm = new FormGroup({
            email : new FormControl('')
        });
    }

    selectMethod(method ?: string) {
        this.method = method;
    }

    focusElem(elem : any) {
        $(elem).focus()
    }

    select(elem : any) {
        $(elem).select();
    }

    savePin() {
        this.updateSequrity({
            security : 'PIN',
            value : `${this.pinForm.value.one}${this.pinForm.value.two}${this.pinForm.value.three}${this.pinForm.value.four}`
        });
    }

    saveEmail() {
        this.updateSequrity({
            security : 'EMAIL',
            value : this.emailForm.value.email
        });
    }

    disableSecutity() {
        this.updateSequrity({
            security : 'DISABLED'
        }, false);
    }

    updateDpp(value : boolean) {
        this.UserService.updateDpp(this.GeomainService.geomain.name || this.GeomainService.geomain.number, value)
            .subscribe(res => {
                console.log(res);
            })
    }

    updateSequrity(data : any, message : boolean = true) {
        this.loader.show();
        data.geomain = this.GeomainService.geomain.name || this.GeomainService.geomain.number;
        this.UserService.updateGeomainSeq(data).subscribe(res => {
            this.loader.hide();
            if(message) {
                if(data.security == 'EMAIL') {
                    this.modalEmail.open();
                }
                else {
                    this.modalPin.open();
                }
            }
            this.GeomainService.geomain.security = data.security;
        }, err => {
            this.loader.hide();
            this.serverError = err.message;
            this.error.open();
        });
    }

}
