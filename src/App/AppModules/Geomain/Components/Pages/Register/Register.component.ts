import {AfterViewInit, Component, OnInit, ViewChild} from "@angular/core";
import {Helpers} from "../../../../../BaseClasses/Helpers";
import {GeoNumberService} from "../../../../../EntityModules/GeoNumber/GeoNumber.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import * as $ from 'jquery';
import {GEONUMBER_CATEGORY} from "../../../../../EntityModules/GeoNumber/GeoNumber.model";
import {MapsAPILoader} from "@agm/core";
import {ValidatorService} from "../../../../../BaseModules/Validation/Validation.service";
import {AuthorizationService} from "../../../../../BaseModules/Authorization/Authorization.service";


@Component({
    selector: 'register-geomain-page',
    templateUrl: './Register.component.html',
    styleUrls : ['./Register.component.less'],
})

export class RegisterComponent implements AfterViewInit {

    @ViewChild('modalStart') private modalStart : any;
    @ViewChild('modal') private modal : any;
    @ViewChild('error') private error : any;
    @ViewChild('limitError') private limitError : any;
    @ViewChild('loader') private loader : any;

    Helpers : Helpers = new Helpers();

    form : FormGroup;

    center : any = {
        lat : 1.3520830,
        lng : 103.8198360
    };

    marker : any = {
        lat : 1.3520830,
        lng : 103.8198360
    };

    modalOptions : any = {
        closeOnConfirm : false,
        closeOnCancel : false,
        closeOnEscape : false,
        closeOnOutsideClick : false
    };

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    geocoder : any;

    addrChanged : boolean = false;
    coordChanged : boolean = false;

    constructor(
        public AuthorizationService : AuthorizationService,
        private ValidatorService : ValidatorService,
        private GeoNumberService : GeoNumberService,
        private mapsApiLoader : MapsAPILoader,
        private Router : Router
    ) {
        this.form = new FormGroup({
            firstName : new FormControl(),
            lastName  : new FormControl(),
            email     : new FormControl(),
            password  : new FormControl(),
            category  : new FormControl(),
            apartment : new FormControl(),
            street    : new FormControl(),
            city      : new FormControl(),
            zip       : new FormControl(),
            country   : new FormControl(),
            lat       : new FormControl(),
            lng       : new FormControl(),
            address   : new FormControl(),
            comment   : new FormControl(),
        });
        this.loadGeocoder();
    }

    ngAfterViewInit() {
        this.modalStart.open();
    }

    clearForm() {
        this.form.patchValue({
            firstName : '',
            lastName  : '',
            email     : '',
            password  : '',
            category  : '',
            apartment : '',
            street    : '',
            city      : '',
            zip       : '',
            country   : '',
            lat       : '',
            lng       : '',
            address   : '',
            comment   : '',
        });
    }

    setAuthData() {
        if(this.AuthorizationService.user) {
            this.form.patchValue({
                firstName : this.AuthorizationService.user.firstName,
                lastName : this.AuthorizationService.user.lastName,
                email : this.AuthorizationService.user.email,
            })
        }
    }

    get showAuthUserForm() {
        if(this.AuthorizationService.user) {
            if(this.form.value.email != this.AuthorizationService.user.email) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return true;
        }
    }

    clickOnMap() {
        this.form.reset({
            apartment : {value : '', disabled : false},
            street    : {value : '', disabled : true},
            city      : {value : '', disabled : true},
            zip       : {value : '', disabled : true},
            country   : {value : '', disabled : true},
            lat       : {value : '', disabled : true},
            lng       : {value : '', disabled : true},
        });
        this.enableGps();
        this.setAuthData();
        this.modalStart.close();
    }

    enterAddress() {
        this.form.reset({
            lat : {value : '', disabled : true},
            lng : {value : '', disabled : true}
        });
        this.enableGps();
        this.setAuthData();
        this.modalStart.close();
    }

    enterCoordinates() {
        this.form.reset({
            apartment : {value : '', disabled : false},
            street    : {value : '', disabled : true},
            city      : {value : '', disabled : true},
            zip       : {value : '', disabled : true},
            country   : {value : '', disabled : true}
        });
        this.enableGps();
        this.setAuthData();
        this.modalStart.close();
    }

    checkAddrIf() {

        if(this.coordChanged) {
            let lat = parseFloat(this.form.value.lat);
            let lng = parseFloat(this.form.value.lng);
            this.getGoogleLocation(lat, lng);
            this.setMapCoordinates(lat, lng);
        }

        if(this.addrChanged) {
            this.checkAddr();
        }
    }

    changeAddr(coord ?: boolean) {
        if(coord) {
            this.coordChanged = true;
            this.addrChanged = false;
        }
        else {
            this.coordChanged = false;
            this.addrChanged = true;
        }
    }

    checkAddr() {
        this.geocoder.geocode({'address' : this.getAddress(true)}, (results : any, status : string) => {
            if(status == 'OK') {
                let lat = results[0].geometry.location.lat();
                let lng = results[0].geometry.location.lng();
                this.form.patchValue({lat, lng});
                this.setMapCoordinates(lat, lng);
                this.coordChanged = false;
                this.addrChanged = false;
            }
            else {
                this.error.open();
            }
        });
    }

    loadGeocoder() {
        this.mapsApiLoader.load().then(() => {
            this.geocoder = new window['google'].maps.Geocoder();
            // this.enableGps();
        })
    }

    setMapCoordinates(lat : number, lng : number) {
        this.center.lat = lat;
        this.center.lng = lng;
        this.setMarkerCoordinates(lat, lng);
    }

    setMarkerCoordinates(lat : number, lng : number) {
        this.marker.lat = lat;
        this.marker.lng = lng;
    }

    enableGps() {
        this.Helpers.getMyLocation((res : any) => {

            this.addrChanged = false;
            this.setMapCoordinates(res.coords.latitude, res.coords.longitude);
            this.getGoogleLocation(res.coords.latitude, res.coords.longitude);
            this.form.patchValue({
                lat : res.coords.latitude,
                lng : res.coords.longitude
            });

        }, (err : any) => {
            this.getLocationByIp();
        })
    }

    getGoogleLocation(lat: number, lng: number) {
        this.geocoder.geocode({'location': {lat, lng}}, (results : any, status : string) => {
            this.form.patchValue({
                apartment : '',
                street : '',
                city : '',
                country : '',
                zip : ''
            });
            this.coordChanged = false;
            this.addrChanged = false;
            console.log(results[0]);
            if(status == 'OK') {
                results[0].address_components.map((item : any) => {
                    if(item.types.indexOf('street_number') >= 0) {
                        this.form.patchValue({ apartment : item.long_name });
                    }
                    if(item.types.indexOf('route') >= 0) {
                        this.form.patchValue({ street : item.long_name });
                    }
                    if(item.types.indexOf('locality') >= 0) {
                        this.form.patchValue({ city : item.long_name });
                    }
                    if(item.types.indexOf('country') >= 0) {
                        this.form.patchValue({ country : item.long_name });
                    }
                    if(item.types.indexOf('postal_code') >= 0) {
                        this.form.patchValue({ zip : item.long_name });
                    }
                });
            }
        });
    }

    getLocationByIp() {
        this.GeoNumberService.getLocation().subscribe(res => {
            this.setMapCoordinates(res.latitude, res.longitude);
            this.getGoogleLocation(res.latitude, res.longitude);
            this.form.patchValue({
                lat     : res.latitude,
                lng     : res.longitude,
                city    : res.city,
                country : res.country_name,
                zip     : res.zip_code,
            });
        })
    }

    mapClick(event : any) {
        this.addrChanged = false;
        this.setMarkerCoordinates(event.coords.lat, event.coords.lng);
        this.getGoogleLocation(event.coords.lat, event.coords.lng);
        this.form.patchValue({
            lat: event.coords.lat,
            lng: event.coords.lng
        });
    }

    get registerData() {
        let data : any = {
            firstName : this.form.controls.firstName.value,
            lastName  : this.form.controls.lastName.value,
            category  : this.form.controls.category.value,
            lat       : this.form.controls.lat.value,
            lon       : this.form.controls.lng.value,
            // apartment : this.form.controls.apartment.value,
            address   : this.getAddress()
        };

        if(this.showAuthUserForm) {
            data.email    = this.form.controls.email.value;
            data.password = this.form.controls.password.value;
        }
        else {
            data.email = this.AuthorizationService.user.email;
            data.password = '111111';
        }

        return data;
    }

    getAddress(string : boolean = false) {
        let addr = '';
        if(this.form.controls.country.value) addr += `${this.form.controls.country.value}, `;
        if(this.form.controls.city.value) addr += `${this.form.controls.city.value}, `;
        if(this.form.controls.street.value) addr += `${this.form.controls.street.value}, `;
        if(this.form.controls.apartment.value) addr += `${this.form.controls.apartment.value}, `;
        if(this.form.controls.zip.value) addr += `${this.form.controls.zip.value}, `;

        if(string) {
            return addr;
        }

        return {
            zip       : this.form.controls.zip.value,
            apartment : this.form.controls.apartment.value,
            street    : this.form.controls.street.value,
            city      : this.form.controls.city.value,
            country   : this.form.controls.country.value,
            comment   : this.form.controls.comment.value
        };
    }

    submit() {
        this.loader.show();
        this.GeoNumberService.register(this.registerData).subscribe(res => {
            this.modal.open();
            this.loader.hide();
        }, err => {

            if(err.code == 1) {
                this.limitError.open();
            }

            this.ValidatorService.addErrorToForm(this.form, err);
            this.loader.hide();
        })
    }

    closeModal() {
        this.modal.close();
        $('body').css('padding-right', 0);
        this.Router.navigate(['/']);
    }

}
