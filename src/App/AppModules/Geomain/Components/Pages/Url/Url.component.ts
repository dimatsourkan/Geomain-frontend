import {Component, OnInit, ViewChild} from "@angular/core";
import {GeomainService} from "../../Geomain.service";
import { AuthorizationService } from '../../../../../BaseModules/Authorization/Authorization.service';
import { UserService } from '../../../../../EntityModules/User/Users.service';
import { GeoNumber } from '../../../../../EntityModules/GeoNumber/GeoNumber.model';


@Component({
    selector: 'geomain-url',
    templateUrl: './Url.component.html',
    styleUrls : ['./Url.component.less'],
})

export class GeomainUrlComponent {

    website : string = null;

    inputShow : boolean = false;

    constructor(
        public GeomainService : GeomainService,
        public UserService : UserService,
        public AS : AuthorizationService
    ) {

    }

    toggleInput() {
        this.inputShow = !this.inputShow;
        this.website   = this.GeomainService.geomain.website;
    }

    updateGeomain(value : string) {

        let geomain = new GeoNumber(this.GeomainService.geomain);
        geomain.website = value;

        this.UserService.updateGeomain(geomain).subscribe(() => {
            this.GeomainService.geomain.website = geomain.website;
        });
    }



}
