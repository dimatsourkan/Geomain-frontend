import {Component} from "@angular/core";
import {GeomainService} from "../../Geomain.service";


@Component({
    selector: 'geomain-coordinates',
    templateUrl: './Coordinates.component.html',
    styleUrls : ['./Coordinates.component.less'],
})

export class GeomainCoordinatesComponent {

    constructor(public GeomainService : GeomainService) {}
}
