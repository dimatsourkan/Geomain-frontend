import {Component} from "@angular/core";
import {Helpers} from "../../../../../BaseClasses/Helpers";
import {GeomainService} from "../../Geomain.service";
import {AuthorizationService} from "../../../../../BaseModules/Authorization/Authorization.service";


@Component({
    selector: 'geomain-page-map',
    templateUrl: './Map.component.html',
    styleUrls : ['./Map.component.less'],
})

export class GeomainMapComponent {

    Helpers : Helpers = new Helpers();

    constructor(
        public GeomainService : GeomainService,
        public AuthorizationService : AuthorizationService
    ) {}
}
