import {Component, ViewChild} from "@angular/core";
import {GeomainService} from "../../Geomain.service";
import {Helpers} from "../../../../../BaseClasses/Helpers";
import {MapsAPILoader} from "@agm/core";
import {GeoNumberService} from "../../../../../EntityModules/GeoNumber/GeoNumber.service";


@Component({
    selector: 'geomain-navigate',
    templateUrl: './Navigate.component.html',
    styleUrls : ['./Navigate.component.less']
})

export class GeomainNavigateComponent {

    @ViewChild('map') private map : any;
    @ViewChild('error') private error : any;

    Helpers : Helpers = new Helpers();

    constructor(
        public GeomainService : GeomainService,
        private mapsApiLoader : MapsAPILoader,
        private GeoNumberService : GeoNumberService
    ) {

    }

    mapReady(map : any) {

        this.mapsApiLoader.load().then(() => {
            let directionsService = new window['google'].maps.DirectionsService();
            let directionsDisplay = new window['google'].maps.DirectionsRenderer();

            directionsDisplay.setMap(map);

            this.GeoNumberService.getLocation().subscribe(res => {

                directionsService.route({
                    origin      : {
                        lat : res.latitude,
                        lng : res.longitude
                    },
                    destination : {
                        lat : this.GeomainService.geomain.lat,
                        lng : this.GeomainService.geomain.lon
                    },
                    travelMode  : 'DRIVING'
                }, (res : any, status : any) => {
                    if(status == 'OK') {
                        directionsDisplay.setDirections(res);
                    }
                    else {
                        this.error.open();
                    }
                });
            });
        })

    }
}
