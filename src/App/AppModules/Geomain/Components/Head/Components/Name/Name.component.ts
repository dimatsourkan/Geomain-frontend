import {Component, Input} from "@angular/core";
import {IGeoNumber} from "../../../../../../EntityModules/GeoNumber/GeoNumber.model";
import {AuthorizationService} from "../../../../../../BaseModules/Authorization/Authorization.service";
import {UserService} from "../../../../../../EntityModules/User/Users.service";


@Component({
    selector: 'geomain-name',
    templateUrl: './Name.component.html',
    styleUrls : ['./Name.component.less'],
})

export class GeomainNameComponent {

    @Input() geonumber : IGeoNumber;

    geomains : IGeoNumber[];

    constructor(
        public UserService : UserService,
        public AuthorizationService : AuthorizationService
    ) {
        if(AuthorizationService.isAuthenticated) {
            this.getGeomains();
        }
    }

    get Path() : string {
        let path = document.location.pathname.split('/');

        return path[path.length-1];
    }

    getGeomains() {
        this.UserService.getGeomains().subscribe(res => {
            this.geomains = res;
        });
    }

}
