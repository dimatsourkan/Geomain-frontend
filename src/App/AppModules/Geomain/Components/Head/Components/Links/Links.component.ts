import {Component, Input} from "@angular/core";
import {GEONUMBER_CATEGORY, IGeoNumber} from "../../../../../../EntityModules/GeoNumber/GeoNumber.model";
import {AuthorizationService} from "../../../../../../BaseModules/Authorization/Authorization.service";


@Component({
    selector: 'geomain-links',
    templateUrl: './Links.component.html',
    styleUrls : ['./Links.component.less'],
})

export class GeomainLinksComponent {

    @Input() geonumber : IGeoNumber;

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    constructor(public AS : AuthorizationService) {}

}
