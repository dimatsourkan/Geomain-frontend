import { AfterViewInit, Component, Input, OnChanges } from '@angular/core';
import { GEONUMBER_CATEGORY, Graphic, IGeoNumber } from '../../../../../../EntityModules/GeoNumber/GeoNumber.model';
import {AuthorizationService} from "../../../../../../BaseModules/Authorization/Authorization.service";
import {UserService} from "../../../../../../EntityModules/User/Users.service";
import {VISITOR_POLICY} from "../../../../../../EntityModules/GeoNumber/visitorPolicy";
import * as $ from 'jquery';


@Component({
    selector: 'geomain-drop',
    templateUrl: './Drop.component.html',
    styleUrls : ['./Drop.component.less'],
})

export class GeomainDropComponent implements OnChanges, AfterViewInit {

    @Input() geonumber : IGeoNumber;

    VISITOR_POLICY : any = VISITOR_POLICY;

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    graphic : Graphic;

    constructor(
        public AuthorizationService : AuthorizationService,
        public UserService : UserService,
    ) {

    }

    changeCategory(category : string) {
        this.UserService.updatePolicy(this.geonumber.name || this.geonumber.number, category).subscribe(res => {
            this.geonumber.visitorPolicy = category;
        })
    }

    ngOnChanges() {
        this.graphic = this.geonumber.getGraphic();
    }

    ngAfterViewInit() {
        $('.graph .edit-list')
            .on("change", '.form-control', () => {
                console.log(this.graphic);
                this.updateGeomain();
            })
            .on("change", '[type="checkbox"]', () => {
                console.log(this.graphic);
                this.updateGeomain();
            })
    }

    updateGeomain() {
        this.geonumber.openTime = JSON.stringify(this.graphic);
        this.UserService.updateGeomain(this.geonumber).subscribe(() => {

        });
    }

}
