import {Component, Input} from "@angular/core";
import {IGeoNumber} from "../../../../../../EntityModules/GeoNumber/GeoNumber.model";
import {AuthorizationService} from "../../../../../../BaseModules/Authorization/Authorization.service";
import {ActivatedRoute} from "@angular/router";


@Component({
    selector: 'geomain-sec-name',
    templateUrl: './SecName.component.html',
    styleUrls : ['./SecName.component.less'],
})

export class GeomainSecNameComponent {

    @Input() geonumber : IGeoNumber;

    number : number;

    constructor(
        public ActivatedRoute : ActivatedRoute,
        public AuthorizationService : AuthorizationService
    ) {
        this.number = this.ActivatedRoute.snapshot.params['number'];
    }

}
