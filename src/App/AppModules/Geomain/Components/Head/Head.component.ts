import {Component, Input} from "@angular/core";
import {IGeoNumber} from "../../../../EntityModules/GeoNumber/GeoNumber.model";


@Component({
    selector: 'geopage-head',
    templateUrl: './Head.component.html',
    styleUrls : ['./Head.component.less'],
})

export class GeopageHeadComponent {

    @Input() geonumber : IGeoNumber;

}
