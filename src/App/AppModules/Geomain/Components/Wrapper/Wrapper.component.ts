import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {GeoNumberService} from "../../../../EntityModules/GeoNumber/GeoNumber.service";
import {GeomainService} from "../Geomain.service";


@Component({
    selector: 'geomain-wrapper',
    templateUrl: './Wrapper.component.html',
    styleUrls : ['./Wrapper.component.less'],
})

export class GeomainWrapperComponent implements OnInit {

    @ViewChild('loader') private loader : any;

    number : number;

    start : boolean = false;

    constructor(
        public Router : Router,
        public ActivatedRoute : ActivatedRoute,
        public GeoNumberService : GeoNumberService,
        public GeomainService : GeomainService
    ) {
        this.number = this.ActivatedRoute.snapshot.params['number'];

        this.ActivatedRoute.params.map(params => params['number']).subscribe(id => {
            this.number = id;

            if(this.start) {
                this.getGeonumber(this.number);
            }

            this.start = true;
        });
    }

    ngOnInit() {
        this.getGeonumber(this.number);
    }

    getGeonumber(geonumber : number, value ?: string) {
        this.loader.show();
        this.GeoNumberService.Code = value;
        this.GeomainService.geomain = null;
        this.GeoNumberService.find(geonumber).subscribe(res => {
            this.GeomainService.geomain = res.data;
            this.loader.hide();
        }, err => {
            this.loader.hide();
            this.Router.navigate(['']);
        });
    }

    submitPin(value : string) {
        this.getGeonumber(this.number, value);
    }
}
