import {Component, ViewChild} from "@angular/core";
import {GeomainService} from "../Geomain.service";
import {GeoNumberService} from "../../../../EntityModules/GeoNumber/GeoNumber.service";
import {PersonalService} from "../../../../Services/PersonalService/PersonalService.service";


@Component({
    selector: 'geomain-timeline',
    templateUrl: './Timeline.component.html',
    styleUrls : ['./Timeline.component.less'],
})

export class GeomainTimelineComponent {

    @ViewChild('area') private area : any;
    @ViewChild('loader') private loader : any;

    constructor(
        private GeoNumberService : GeoNumberService,
        private GeomainService : GeomainService,
        private PersonalService : PersonalService
    ) {

    }

    get AreaLength() {
        return this.area.nativeElement.value.length;
    }

    submit() {

        if(!this.area.nativeElement.value) {
            return;
        }

        this.loader.show();
        this.PersonalService.addPostMessage(this.GeomainService.geomain.name || this.GeomainService.geomain.number, this.area.nativeElement.value)
            .subscribe(res => {
                this.getGeonumber(this.GeomainService.geomain.name || this.GeomainService.geomain.number);
            })
    }

    getGeonumber(geonumber : any) {
        this.GeoNumberService.find(geonumber).subscribe(res => {
            this.GeomainService.geomain = res.data;
            this.area.nativeElement.value = '';
            this.loader.hide();
        }, err => {
            this.loader.hide();
        });
    }

}
