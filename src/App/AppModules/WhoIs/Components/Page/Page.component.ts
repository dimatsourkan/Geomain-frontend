import {Component} from "@angular/core";
import {GEONUMBER_CATEGORY} from "../../../../EntityModules/GeoNumber/GeoNumber.model";


@Component({
    selector: 'who-is-page',
    templateUrl: './Page.component.html',
    styleUrls : ['./Page.component.less']
})

export class WhoIsPageComponent {

    whoIs : any;

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    date : Date = new Date();

    onFind(result : { value : string, result : any }) {
        this.whoIs = result.result;
    }

}
