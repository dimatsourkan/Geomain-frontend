import {WhoIsPageComponent} from "./Components/Page/Page.component";
import {RouterModule} from "@angular/router";

export const routes = [
    {
        path: '',
        component : WhoIsPageComponent
    },
];

export const WHO_IS_ROUTING = RouterModule.forChild(routes);