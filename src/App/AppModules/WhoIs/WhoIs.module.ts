import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {WhoIsPageComponent} from "./Components/Page/Page.component";
import {WHO_IS_ROUTING} from "./WhoIs.routing";
import {SearchModule} from "../../BaseModules/Search/Search.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        LoaderModule,
        SearchModule,

        WHO_IS_ROUTING
    ],
    declarations: [
        WhoIsPageComponent
    ],
    exports : [
    ],
    providers: []
})

export class WhoIsModule {

}
