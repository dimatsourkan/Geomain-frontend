import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {PrivacyPageComponent} from "./Components/Page/Page.component";
import {PRIVACY_ROUTING} from "./Privacy.routing";
import {AccordionModule} from "ngx-accordion";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        LoaderModule,
        AccordionModule,

        PRIVACY_ROUTING
    ],
    declarations: [
        PrivacyPageComponent
    ],
    exports : [
    ],
    providers: []
})

export class PrivacyModule {

}
