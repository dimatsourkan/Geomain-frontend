import {PrivacyPageComponent} from "./Components/Page/Page.component";
import {RouterModule} from "@angular/router";

export const routes = [
    {
        path: '',
        component : PrivacyPageComponent
    },
];

export const PRIVACY_ROUTING = RouterModule.forChild(routes);