import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {TranslateModule} from "@ngx-translate/core";
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {GeomarketWrapperComponent} from "./Components/Wrapper/Wrapper.component";
import {GEOMARKET_ROUTES} from "./Geomarket.routing";
import {AppHttpModule} from "../../BaseModules/Http/Http.module";
import { GeomarketHeadComponent } from './Components/Head/Head.component';
import { GeomarketBrowseComponent } from './Components/Pages/Browse/Browse.component';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        RouterModule,
        TranslateModule,
        LoaderModule,
        AppHttpModule,

        GEOMARKET_ROUTES

    ],
    declarations: [
        GeomarketWrapperComponent,
        GeomarketHeadComponent,
        GeomarketBrowseComponent
    ],
    exports : [

    ],
    providers: [
    ]
})

export class GeomarketModule {

}
