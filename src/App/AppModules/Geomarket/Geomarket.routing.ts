import {RouterModule} from "@angular/router";
import { GeomarketBrowseComponent } from './Components/Pages/Browse/Browse.component';
import { GeomarketWrapperComponent } from './Components/Wrapper/Wrapper.component';

export const routes = [

    {
        path : '',
        component : GeomarketWrapperComponent,
        children : [

            {
                path: 'browse',
                component : GeomarketBrowseComponent
            },

            {
                path: '',
                redirectTo : 'browse'
            },

        ]
    }
];

export const GEOMARKET_ROUTES = RouterModule.forChild(routes);
