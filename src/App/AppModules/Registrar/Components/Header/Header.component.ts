import { Component } from '@angular/core';
import {AuthorizationService} from "../../../../BaseModules/Authorization/Authorization.service";

@Component({
    selector: 'registrar-header',
    templateUrl: './Header.component.html',
    styleUrls : ['./Header.component.less']
})

export class RegistrarHeaderComponent {
    constructor(public AS : AuthorizationService) {}
}
