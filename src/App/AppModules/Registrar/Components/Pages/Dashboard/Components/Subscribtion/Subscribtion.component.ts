import {Component, OnInit, ViewChild} from '@angular/core';
import { RegistrarService } from '../../../../../Registrar.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'registrar-dashboard-subscribtion',
    templateUrl: './Subscribtion.component.html',
    styleUrls : ['./Subscribtion.component.less']
})

export class RegistrarDashboardSubscribtionComponent {

    form : FormGroup;

    geoname : string;

    prices : any = [];

    constructor(
        private RegistrarService : RegistrarService,
        private ActivatedRoute : ActivatedRoute,
        private Router : Router
    ) {

        this.ActivatedRoute.queryParams.subscribe(res => {
            this.geoname = res.geoname;

            this.form = new FormGroup({
                customerNumber : new FormControl(''),
                geomainName : new FormControl(this.geoname),
                months : new FormControl('1'),
                type : new FormControl(res.type)
            });

            this.getNumber();
        });

        this.getDiscounts();
    }

    getDiscounts() {
        this.RegistrarService.priceList().subscribe(res => {
            this.prices = res.productPrices;
        })
    }

    getNumber() {
        this.RegistrarService.getCustomerNumber(this.geoname).subscribe(res => {
            this.form.patchValue({
                customerNumber : res.customerNumber
            })
        }, err => {
            window.history.back();
        })
    }

    get price() {

        let type  = this.form.value.type;
        let count = this.form.value.months;
        let price = this.prices.find((item : any) => {
            return item.product == type;
        });

        if(!type || !price) return 0;

        return price.discountPrice * count;

    }

    submit() {
        this.RegistrarService.addSubscription(this.form.value).subscribe(res => {
            this.Router.navigate(['/registrar', 'manage', this.geoname]);
        })
    }



}
