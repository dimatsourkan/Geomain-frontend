import {Component, OnInit, ViewChild} from '@angular/core';
import { RegistrarService } from '../../../../../Registrar.service';

@Component({
    selector: 'registrar-dashboard-main',
    templateUrl: './Main.component.html',
    styleUrls : ['./Main.component.less']
})

export class RegistrarDashboardMainComponent implements OnInit {

    @ViewChild('loader') private loader : any;

    data : any;

    period : string = 'today';

    chartData : any;

    constructor(private RegistrarService : RegistrarService) {

    }

    getData() {
        this.loader.show();
        this.RegistrarService.dashboardSummary(this.period).subscribe(res => {
            this.data = res;
            this.chartData = [
                res.countNumberGeomain,
                parseFloat(res.amountDebitedAddons)
            ];
            this.loader.hide();
        }, () => this.loader.hide());
    }

    ngOnInit() {
        this.getData();
    }

}
