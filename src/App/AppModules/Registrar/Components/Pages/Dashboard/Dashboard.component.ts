import {Component} from '@angular/core';

@Component({
    selector: 'registrar-dashboard',
    template: `<router-outlet></router-outlet>`
})

export class RegistrarDashboardComponent {

}
