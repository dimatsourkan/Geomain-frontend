import {Component, ViewChild} from '@angular/core';
import {RegistrarService} from "../../../Registrar.service";
import {GEONUMBER_CATEGORY} from "../../../../../EntityModules/GeoNumber/GeoNumber.model";

@Component({
    selector: 'registrar-register-renew',
    templateUrl: './Renew.component.html',
    styleUrls : ['./Renew.component.less']
})

export class RegistrarRenewComponent {

    @ViewChild('modal') private modal : any;
    @ViewChild('loader') private loader : any;
    @ViewChild('userError') private userError : any;
    @ViewChild('preloader') private preloader : any;

    SEARCH_TYPES = {
        CN : 'CUSTOMER_NUMBER',
        GN : 'GEOMAIN'
    };

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    search : string = '';

    type : string = this.GEONUMBER_CATEGORY.CN;

    nameAvailable : boolean = false;

    nameAvailableErr : boolean = false;

    geomain : any;

    prices : any = {};

    constructor(
        private RegistrarService : RegistrarService
    ) {
        this.getPriceList();
    }

    setDefaultGeomain() {
        this.geomain = {
            customerNumber : '',
            geoName : '',
            geoNumber : '',
            category : '',
            term : '1',
            id : ''
        };
    }

    get categoryPrice() {
        if(!this.prices.PGN) return 0;
        let categoryPrice = 0;
        switch(this.geomain.category) {
            case this.GEONUMBER_CATEGORY.PERSONAL   : categoryPrice = this.prices.PGN.discountPrice; break;
            case this.GEONUMBER_CATEGORY.BUSINESS   : categoryPrice = this.prices.BGN.discountPrice; break;
            case this.GEONUMBER_CATEGORY.GOVERNMENT : categoryPrice = this.prices.GGN.discountPrice; break;
        }
        return categoryPrice;
    }

    get geomainPrice() {
        if(!this.prices.PGN) return 0;
        let geonamePrice = 0;
        switch(this.geomain.geoName.length) {
            case 2 : geonamePrice = this.prices.SR2.discountPrice; break;
            case 3 : geonamePrice = this.prices.SR3.discountPrice; break;
            case 4 : geonamePrice = this.prices.SR4.discountPrice; break;
            case 5 : geonamePrice = this.prices.SR5.discountPrice; break;
        }
        return geonamePrice;

    }

    get price() {

        if(!this.prices.PGN) return;

        return this.geomain.term * (this.categoryPrice + this.geomainPrice);
    }

    getPriceList() {
        this.RegistrarService.priceList().subscribe(res => {
            for(let i in res) {
                this.prices[res[i].product] = res[i];
            }
        });
    }

    find() {

        if(!this.search) return;

        this.loader.show();
        this.RegistrarService.search(this.SEARCH_TYPES.GN, this.search).subscribe(res => {
            if(res.length && res.length == 1) {
                this.nameAvailable = true;
                this.setGeomainData(res[0]);
            }
            else {
                this.nameAvailableErr = true;
            }
            this.loader.hide();
        }, () => {
            this.nameAvailableErr = true;
            this.loader.hide();
        })
    }

    setGeomainData(geomain : any) {
        this.geomain = {
            customerNumber : geomain.customerNumber,
            geoName : geomain.geoName,
            geoNumber : geomain.geoNumber,
            category : geomain.category,
            term : 1,
            id : geomain.id
        };
    }

    submit() {
        this.loader.show();
        console.log(this.geomain);
        this.RegistrarService.updateExpiry(this.geomain).subscribe(res => {
            this.modal.open();
            this.loader.hide();
        }, err => {
            this.loader.hide();
        })
    }

    clear() {
        this.search = '';
        this.nameAvailable = false;
        this.nameAvailableErr = false;
        this.setDefaultGeomain();
    }


}
