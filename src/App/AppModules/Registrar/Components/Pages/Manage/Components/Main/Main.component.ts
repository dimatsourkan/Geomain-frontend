import {Component, ViewChild} from '@angular/core';
import {RegistrarService} from "../../../../../Registrar.service";



@Component({
    selector: 'registrar-manage-main',
    templateUrl: './Main.component.html',
    styleUrls : ['./Main.component.less']
})

export class RegistrarManageMainComponent {

    @ViewChild('loader') private loader : any;

    SEARCH_TYPES = {
        RE : 'REGISTRANT_EMAIL',
        CN : 'CUSTOMER_NUMBER',
        GN : 'GEOMAIN',
        DT : 'DATE',
        CI : 'CSR_ID'
    };

    type : string = this.SEARCH_TYPES.RE;

    findedType : string = this.type;

    search : string = '';

    data : any = [];

    constructor(private RegistrarService : RegistrarService) {

    }

    find() {
        this.loader.show();
        this.RegistrarService.search(this.type, this.search).subscribe(res => {
            this.data = res;
            this.findedType = this.type;
            this.loader.hide();
        }, () => {
            this.loader.hide();
        })
    }
}
