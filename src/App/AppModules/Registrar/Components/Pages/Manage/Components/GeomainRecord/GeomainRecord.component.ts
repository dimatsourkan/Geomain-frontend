import {Component, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {GEONUMBER_CATEGORY, IGeoNumber} from "../../../../../../../EntityModules/GeoNumber/GeoNumber.model";
import {GeoNumberService} from "../../../../../../../EntityModules/GeoNumber/GeoNumber.service";
import {ActivatedRoute, Router} from "@angular/router";
import {RegistrarService} from "../../../../../Registrar.service";
import {ValidatorService} from "../../../../../../../BaseModules/Validation/Validation.service";

@Component({
    selector: 'registrar-manage-geomain-record',
    templateUrl: './GeomainRecord.component.html',
    styleUrls : ['./GeomainRecord.component.less']
})

export class RegistrarManageGeomainRecordComponent {

    @ViewChild('modal') private modal : any;
    @ViewChild('loader') private loader : any;

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    form: FormGroup;

    number : string;

    geomain : IGeoNumber;

    subscriptions : any;

    constructor(
        private RegistrarService : RegistrarService,
        private GeoNumberService : GeoNumberService,
        private Router : Router,
        private ActivatedRoute : ActivatedRoute,
        private ValidatorService : ValidatorService
    ) {
        this.number = this.ActivatedRoute.snapshot.params['number'];
        this.getGeomain();
        this.setForm();
    }

    adSubscribtion(type : string) {
        this.Router.navigate(['/registrar','dashboard','ad-subscribtion'], {
            queryParams : {
                geoname : this.geomain.name,
                type : type
            }
        })
    }

    getGeomain() {
        this.GeoNumberService.find(this.number).subscribe(res => {
            this.geomain = res;
            this.getGnameSubscriptions(res);
            this.setGeomain(this.geomain);
        }, () => {
            this.Router.navigate(['/']);

        });
    }

    getGnameSubscriptions(geomain : any) {
        this.RegistrarService.getGnameSubscriptions(geomain.name).subscribe(res => {
            this.subscriptions = res;
        })
    }

    setGeomain(geomain : IGeoNumber) {
        this.form.reset({
            number     : { value : geomain.owner.fullName, disabled : true },
            registrant : { value : geomain.owner.fullName, disabled : true },
            registered : { value : geomain.owner.fullName, disabled : true },
            email      : { value : geomain.owner.email, disabled : true },
            category   : { value : geomain.category, disabled : true },
            phone      : { value : geomain.owner.businessPhone, disabled : true },
            geoname    : { value : geomain.name, disabled: true } ,
            lang       : { value : 'En', disabled : true },
            geonumber  : { value : geomain.number, disabled : false },
            created    : { value : geomain.created, disabled : true },
            expires    : { value : geomain.expiryDate, disabled : true },
        })
    }

    setForm() {
        this.form = new FormGroup({
            number     : new FormControl(''),
            registrant : new FormControl(''),
            registered : new FormControl(''),
            email      : new FormControl(''),
            category   : new FormControl(''),
            phone      : new FormControl(''),
            geoname    : new FormControl(''),
            lang       : new FormControl(''),
            geonumber  : new FormControl(''),
            created    : new FormControl(''),
            expires    : new FormControl(''),
        });
    }

    back() {
        window.history.back();
    }

    update() {
        this.loader.show();
        this.RegistrarService.updateGeoname({ id : this.geomain.id, geonumber : this.form.value.geonumber }).subscribe(res => {
            this.loader.hide();
            if(this.geomain.number != this.form.value.geonumber) {
                this.modal.open();
            }
        }, err => {
            this.loader.hide();
            this.ValidatorService.addErrorToForm(this.form, err);
        })
    }

}
