import {Component, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {CODES} from "../../../../../../AppAuthorization/Components/Registrar/codes";
import {RegistrarService} from "../../../../../Registrar.service";
import {ValidatorService} from "../../../../../../../BaseModules/Validation/Validation.service";
import {Router} from "@angular/router";

@Component({
    selector: 'registrar-manage-registrant-create',
    templateUrl: './CreateRegistrant.component.html',
    styleUrls : ['./CreateRegistrant.component.less']
})

export class RegistrarManageRegistrantCreateComponent {

    @ViewChild('modal') private modal : any;
    @ViewChild('loader') private loader : any;

    CODES : any = CODES;

    form: FormGroup;

    showForm : false;

    notError : boolean = true;

    congratulations : boolean = false;

    email : string;

    user : any;

    constructor(
        public RegistrarService : RegistrarService,
        public ValidatorService : ValidatorService,
        private Router : Router
    ) {

        this.setForm();

    }

    setForm() {
        this.form = new FormGroup({
            firstName   : new FormControl(''),
            lastName    : new FormControl(''),
            email       : new FormControl(''),
            phoneCode   : new FormControl(''),
            phoneNum    : new FormControl(''),
            mobilePhone : new FormControl(''),
            company     : new FormControl(false),
            companyName : new FormControl(''),
            companyUrl  : new FormControl(''),
            password    : new FormControl('')
        });
    }

    checkEmail() {

        if(!this.email) return;

        this.loader.show();
        this.RegistrarService.checkEmail(this.email).subscribe(res => {
            this.form.patchValue({ email : this.email });
            this.showForm = res.success;
            this.notError = res.success;
            if(!this.showForm) {
                this.modal.open();
            }
            this.loader.hide();
        }, err => {
            this.loader.hide();
        })
    }

    createUser() {
        this.form.patchValue({
            mobilePhone : this.form.value.phoneCode + this.form.value.phoneNum
        });

        this.loader.show();
        this.RegistrarService.createUser(this.form.value).subscribe(res => {
            this.congratulations = true;
            this.user = res;
            this.loader.hide();
        }, err => {
            this.ValidatorService.addErrorToForm(this.form, err);
            this.loader.hide();
        })
    }

    resetScreen() {
        this.form.reset();
        this.congratulations = false;
        this.showForm = false;
        this.email = '';
    }

    toRegisterGeoname() {
        this.Router.navigate(['/registrar/register'], { queryParams : { email : this.email } })
    }

}
