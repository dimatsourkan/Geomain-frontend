import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {RegistrarService} from "../../../../../Registrar.service";
import {AuthorizationService} from "../../../../../../../BaseModules/Authorization/Authorization.service";
import {ValidatorService} from "../../../../../../../BaseModules/Validation/Validation.service";

@Component({
    selector: 'registrar-manage-registrant-record',
    templateUrl: './RegistrantRecord.component.html',
    styleUrls : ['./RegistrantRecord.component.less']
})

export class RegistrarManageRegistrantRecordComponent implements OnInit {

    @ViewChild('modal') private modal : any;
    @ViewChild('loader') private loader : any;
    @ViewChild('preloader') private preloader : any;

    form: FormGroup;

    id : string;

    user : any;

    geomains : any[];

    constructor(
        private AuthService : AuthorizationService,
        private ActivatedRoute : ActivatedRoute,
        private RegistrarService : RegistrarService,
        public ValidatorService : ValidatorService
    ) {

        this.id = this.ActivatedRoute.snapshot.params['id'];
        this.setForm();

    }

    ngOnInit() {
        this.findUser();
    }

    getGeomains(num : any) {
        this.RegistrarService.getUserGeonames(num).subscribe(res => {
            this.geomains = res.data;
        });
    }

    findUser() {
        this.preloader.show();
        this.RegistrarService.findUser(this.id).subscribe(res => {
            this.user = res;
            this.getGeomains(this.user.customerNumber);
            this.preloader.hide();
            this.patchForm();
        }, err => {
            this.preloader.hide();
        })
    }

    patchForm() {
        this.form.reset({
            customerNumber   : { value : this.user.customerNumber, disabled : true },
            registrant       : { value : this.AuthService.user.fullName, disabled : true },
            registrationDate : { value : this.user.registrationDate, disabled : true },
            email            : this.user.email,
            mobilePhone      : this.user.mobilePhone,
            companyUrl       : this.user.companyUrl
        })
    }

    setForm() {
        this.form = new FormGroup({
            customerNumber   : new FormControl(''),
            registrant       : new FormControl(''),
            registrationDate : new FormControl(''),
            email            : new FormControl(''),
            mobilePhone      : new FormControl(''),
            companyUrl       : new FormControl(''),
        });

    }

    goBack() {
        window.history.back();
    }

    update() {

        let oldEmail = this.user.email;

        this.user.email       = this.form.value.email;
        this.user.mobilePhone = this.form.value.mobilePhone;
        this.user.companyUrl  = this.form.value.companyUrl;

        this.loader.show();
        this.RegistrarService.updateUser(this.user).subscribe(res => {
            this.loader.hide();
            if(oldEmail != this.user.email) {
                this.modal.open();
            }
        }, err => {
            this.ValidatorService.addErrorToForm(this.form, err);
            this.loader.hide();
        });
    }

}
