import {Component, OnInit, ViewChild} from '@angular/core';
import {RegistrarService} from "../../../../../Registrar.service";

@Component({
    selector: 'registrar-account-dashboard',
    templateUrl: './Dashboard.component.html',
    styleUrls : ['./Dashboard.component.less']
})

export class RegistrarAccountDashboardComponent implements OnInit {

    @ViewChild('loader') private loader : any;

    page  : number = 1;
    total : number = 0;
    size  : number = 10;

    data : any[];

    constructor(private RegistrarService : RegistrarService) {

    }

    ngOnInit() {
        this.getData();
    }

    onChangePage(page : number = 1) {
        this.page = page;
        this.getData();
    }

    getData() {
        this.loader.show();
        this.RegistrarService.allTransactions(this.page, this.size).subscribe(res => {
            this.page  = res.page;
            this.data  = res.data;
            this.total = this.getTotalPages(res.totalElements);
            this.loader.hide();
        });
    }

    getTotalPages(totalElements : number) : number {
        return Math.ceil(totalElements/this.size);
    }

}
