import {Component, ViewChild} from '@angular/core';
import {RegistrarService} from "../../../../../../../Registrar.service";
import {FormControl, FormGroup} from "@angular/forms";
import {ValidatorService} from "../../../../../../../../../BaseModules/Validation/Validation.service";

@Component({
    selector: 'registrar-account-add-funds-methods',
    templateUrl: './Methods.component.html',
    styleUrls : ['./Methods.component.less']
})

export class RegistrarAccountAddFundsMethodsComponent {

    @ViewChild('loader') private loader : any;
    @ViewChild('modal') private modal : any;
    @ViewChild('errorModal') private errorModal : any;

    form : FormGroup;

    error : string;

    constructor(
        private RegistrarService : RegistrarService,
        private ValidatorService : ValidatorService
    ) {

        this.form = new FormGroup({
            paymentType : new FormControl('BANK'),
            bankAccountNumber : new FormControl(''),
            swiftcode : new FormControl(''),
            city : new FormControl(''),
            address : new FormControl(''),
            iban : new FormControl(''),
        });
    }

    create() {
        this.loader.show();
        this.RegistrarService.paymentCreate(this.form.value).subscribe(res => {
            this.form.reset({paymentType : this.form.value.paymentType});
            this.modal.open();
            this.loader.hide();
        }, err => {
            this.loader.hide();
            this.ValidatorService.addErrorToForm(this.form, err);
            if(err.message) {
                this.errorModal.open();
                this.error = err.message;
            }
        });
    }

}
