import {Component, OnInit, ViewChild} from '@angular/core';
import {RegistrarService} from "../../../../../Registrar.service";

@Component({
    selector: 'registrar-account-discount',
    templateUrl: './Discount.component.html',
    styleUrls : ['./Discount.component.less']
})

export class RegistrarAccountDiscountComponent implements OnInit {

    @ViewChild('loader') private loader : any;

    data : any;

    plans : any;

    constructor(private RegistrarService : RegistrarService) {}

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.loader.show();

        this.RegistrarService.priceList().subscribe(res => {

            this.transformPrices(res);

            this.loader.hide();
        }, () => this.loader.hide())
    }

    transformPrices(prices : any) {

        console.log(prices);

        this.plans = {};

        for(let i in prices.discounts) {
            this.plans[prices.discounts[i].plan] = {
                min : this.addComma(prices.discounts[i].min),
                max : this.addComma(prices.discounts[i].max)
            };
            this.plans[prices.discounts[i].plan].products = {};
        }


        for(let i in this.plans) {

            for(let j in prices.productPrices) {

                let product = prices.productPrices;
                let name    = product[j].product;

                let price : any = (product[j].price - (product[j].price*product[j].discounts[i]/100)).toFixed(2);

                this.plans[i].products[name] = {
                    discountPrice : product[j].discountPrice,
                    discount : product[j].discounts[i],
                    product : product[j].product,
                    price : price,
                };

            }
        }

    }

    addComma(n : number = 0) {
        let commaNum = [];
        let num : string = n.toString();

        let addComma = 0;
        for(let i = num.length; i >= 0; i--) {
            commaNum.unshift(num[i]);
            if(addComma == 3) {
                addComma = 1;
                if(num[i-1]) {
                    commaNum.unshift(',');
                }
            }
            else {
                addComma++;
            }
        }

        return commaNum.join('');
    }

}
