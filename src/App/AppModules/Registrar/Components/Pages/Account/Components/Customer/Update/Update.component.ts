import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {RegistrarService} from "../../../../../../Registrar.service";
import {ValidatorService} from "../../../../../../../../BaseModules/Validation/Validation.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'registrar-account-customer',
    templateUrl: './Update.component.html',
    styleUrls : ['./Update.component.less']
})

export class RegistrarAccountCustomerUpdateComponent implements OnInit {

    @ViewChild('loader') private loader : any;

    form : FormGroup;

    csrId : number;

    csr : any;

    constructor(
        private ActivatedRoute : ActivatedRoute,
        private RegistrarService : RegistrarService,
        public ValidatorService : ValidatorService,
        private Router : Router
    ) {

        this.csrId = this.ActivatedRoute.snapshot.params['id'];

        this.form = new FormGroup({
            firstName   : new FormControl(''),
            lastName    : new FormControl(''),
            mobilePhone  : new FormControl(''),
            email  : new FormControl(''),
            id  : new FormControl(''),
        });

    }

    patchForm() {
        this.form.patchValue({
            firstName : this.csr.firstName,
            lastName : this.csr.lastName,
            mobilePhone : this.csr.mobilePhone,
            email : this.csr.email,
            id : this.csr.id
        });
    }

    ngOnInit() {
        this.getCsr();
    }

    getCsr() {
        this.loader.show();
        this.RegistrarService.getCsrById(this.csrId).subscribe(res => {
            this.csr = res;
            this.patchForm();
            this.loader.hide();
        });
    }

    submit() {
        this.loader.show();
        console.log(this.form.value);
        this.RegistrarService.updateCsr(this.csr.id, this.form.value).subscribe(() => {
            this.loader.hide();
            this.Router.navigate(['/registrar/account/customer']);
        }, err => {
            this.ValidatorService.addErrorToForm(this.form, err);
            this.loader.hide();
        })
    }

    uploadAvatar(form : any) {
        this.loader.show();
        this.RegistrarService.uploadCsrPhoto(this.csr.id, new FormData(form)).subscribe(() => {
            this.loader.hide();
            this.getCsr();
        }, err => {
            this.loader.hide();
        });
    }

    removeAvatar() {
        this.loader.show();
        this.RegistrarService.deleteCsrPhoto(this.csr.id).subscribe(res => {
            this.csr.photoLink = null;
            this.loader.hide();
        },(err : any) => {
            this.loader.hide();
        });
    }

}