import {Component, ViewChild} from '@angular/core';
import {AuthorizationService} from "../../../../../../../BaseModules/Authorization/Authorization.service";
import {FormControl, FormGroup} from "@angular/forms";
import {RegistrarService} from "../../../../../Registrar.service";
import {ValidatorService} from "../../../../../../../BaseModules/Validation/Validation.service";

@Component({
    selector: 'registrar-account-profile',
    templateUrl: './Profile.component.html',
    styleUrls : ['./Profile.component.less']
})

export class RegistrarAccountProfileComponent {

    @ViewChild('loader') private loader : any;

    form : FormGroup;

    constructor(
        private ValidatorService : ValidatorService,
        private AuthorizationService : AuthorizationService,
        private RegistrarService : RegistrarService
    ) {
        this.form = new FormGroup({
            firstName         : new FormControl(this.AuthorizationService.user.firstName),
            lastName          : new FormControl(this.AuthorizationService.user.lastName),
            designation       : new FormControl(this.AuthorizationService.user.designation),
            geomain           : new FormControl(this.AuthorizationService.user.geomain),
            website           : new FormControl(this.AuthorizationService.user.website),
            businessPhone     : new FormControl(this.AuthorizationService.user.businessPhone),
            businessName      : new FormControl({value:this.AuthorizationService.user.businessName,disabled:true}),
            businessRegNumber : new FormControl({value:this.AuthorizationService.user.businessRegNumber,disabled:true}),
            email             : new FormControl({value:this.AuthorizationService.user.email,disabled:true}),
            mobilePhone       : new FormControl({value:'',disabled: true}),
            country           : new FormControl({value:'',disabled : true})
        })
    }

    submit() {
        this.loader.show();
        this.RegistrarService.update(this.form.value).subscribe(() => {
            this.loader.hide();
        }, err => {
            this.loader.hide();
            this.ValidatorService.addErrorToForm(this.form, err);
        })
    }

}
