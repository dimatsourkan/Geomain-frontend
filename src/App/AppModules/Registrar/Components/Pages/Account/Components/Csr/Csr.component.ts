import {Component, OnInit, ViewChild} from '@angular/core';
import {RegistrarService} from "../../../../../Registrar.service";

@Component({
    selector: 'registrar-account-csr',
    templateUrl: './Csr.component.html',
    styleUrls : ['./Csr.component.less']
})

export class RegistrarAccountCsrComponent implements OnInit {

    @ViewChild('loader') private loader : any;
    @ViewChild('confirm') private confirm : any;

    page  : number = 1;
    total : number = 0;
    size  : number = 10;

    search : string = '';

    data : any[];

    currentCsr : any;

    constructor(private RegistrarService : RegistrarService) {

    }

    ngOnInit() {
        this.getData();
    }

    onChangePage(page : number = 1) {
        this.page = page;
        this.getData();
    }

    getData() {
        this.loader.show();
        this.RegistrarService.searchCsr(this.search, this.page).subscribe(res => {
            this.page  = res.page;
            this.data  = res.data;
            this.total = this.getTotalPages(res.totalElements);
            this.loader.hide();
        });
    }

    getTotalPages(totalElements : number) : number {
        return Math.ceil(totalElements/this.size);
    }

    toDelete(csr : any) {
        this.currentCsr = csr;
        this.confirm.open();
    }

    removeCsr() {
        this.confirm.close();
        this.RegistrarService.deleteCsr(this.currentCsr.id).subscribe(res => {
            this.getData();
        })
    }

}
