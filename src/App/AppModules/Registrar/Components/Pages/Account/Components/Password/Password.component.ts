import {Component, ViewChild} from '@angular/core';
import {UserService} from "../../../../../../../EntityModules/User/Users.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Helpers} from "../../../../../../../BaseClasses/Helpers";
import { PersonalService } from '../../../../../../../Services/PersonalService/PersonalService.service';

@Component({
    selector: 'registrar-account-password',
    templateUrl: './Password.component.html',
    styleUrls : ['./Password.component.less']
})

export class RegistrarAccountPasswordComponent {

    @ViewChild('loader') private loader : any;
    @ViewChild('error') private error   : any;
    @ViewChild('modal') private modal   : any;

    serverError : string;

    form : FormGroup;

    Helpers : Helpers;

    constructor(
        private UserService : UserService,
        public PersonalService : PersonalService
    ) {

        this.Helpers = new Helpers();

        this.form = new FormGroup({
            currentPassword : new FormControl(''),
            newPassword     : new FormControl('')
        });
    }

    change() {
        this.loader.show();
        this.PersonalService.changePass(this.form.value.currentPassword, this.form.value.newPassword)
            .subscribe(res => {
                this.loader.hide();
                this.form.patchValue({
                    currentPassword : '',
                    newPassword : ''
                });
                this.modal.open();
        }, err => {
                this.loader.hide();
                this.serverError = err.message;
                this.error.open();
            });
    }

}
