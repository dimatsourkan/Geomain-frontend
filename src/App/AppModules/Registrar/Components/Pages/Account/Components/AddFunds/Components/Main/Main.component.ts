import {Component, ViewChild} from '@angular/core';
import {RegistrarService} from "../../../../../../../Registrar.service";

@Component({
    selector: 'registrar-account-add-funds',
    templateUrl: './Main.component.html',
    styleUrls : ['./Main.component.less']
})

export class RegistrarAccountAddFundsMainComponent {

    @ViewChild('loader') private loader : any;
    @ViewChild('error') private error : any;

    summ : any;

    serverError : string;

    constructor(private RegistrarService : RegistrarService) {}

    pay() {

        if(!this.summ) return;

        this.loader.show();

        this.RegistrarService.createDeposit(this.summ).subscribe(res => {
            document.location.href = res.payPalRedirect;
        }, err => {
            this.loader.hide();
            this.serverError = err.message;
            this.error.open();
        });
    }

}
