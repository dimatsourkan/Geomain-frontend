import {Component} from '@angular/core';

@Component({
    selector: 'registrar-account-add-funds',
    template: `<router-outlet></router-outlet>`
})

export class RegistrarAccountAddFundsComponent {}
