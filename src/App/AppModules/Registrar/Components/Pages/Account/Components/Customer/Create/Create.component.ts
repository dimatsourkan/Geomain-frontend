import {Component, ViewChild} from '@angular/core';
import {AuthorizationService} from "../../../../../../../../BaseModules/Authorization/Authorization.service";
import {FormControl, FormGroup} from "@angular/forms";
import {UserService} from "../../../../../../../../EntityModules/User/Users.service";
import {CODES} from "../../../../../../../AppAuthorization/Components/Registrar/codes";
import {Helpers} from "../../../../../../../../BaseClasses/Helpers";
import {RegistrarService} from "../../../../../../Registrar.service";
import {ValidatorService} from "../../../../../../../../BaseModules/Validation/Validation.service";

import * as $ from 'jquery';
import {Router} from "@angular/router";

@Component({
    selector: 'registrar-account-customer',
    templateUrl: './Create.component.html',
    styleUrls : ['./Create.component.less']
})

export class RegistrarAccountCustomerCreateComponent {

    @ViewChild('loader') private loader : any;

    Helpers : Helpers = new Helpers();

    form : FormGroup;

    CODES : any = CODES;

    imagePrev : string;

    constructor(
        public AS : AuthorizationService,
        private UserService : UserService,
        private RegistrarService : RegistrarService,
        public ValidatorService : ValidatorService,
        private Router : Router
    ) {

        this.form = new FormGroup({
            firstName   : new FormControl(''),
            lastName    : new FormControl(''),
            email       : new FormControl(''),
            mobilePhone : new FormControl(''),
            password    : new FormControl(''),
            phoneCode   : new FormControl(''),
            phoneNum    : new FormControl(''),
        });

    }

    getForm(formNative : any) {
        let form = new FormData(formNative);
        form.append('firstName',   this.form.value.firstName);
        form.append('lastName',    this.form.value.lastName);
        form.append('email',       this.form.value.email);
        form.append('mobilePhone',this.form.value.phoneCode+this.form.value.phoneNum);
        form.append('password',    this.form.value.password);
        return form;
    }

    submit(formNative : any) {
        this.loader.show();
        this.RegistrarService.createCsr(this.getForm(formNative)).subscribe(() => {
            this.loader.hide();
            this.Router.navigate(['/registrar/account/customer']);
        }, err => {
            this.ValidatorService.addErrorToForm(this.form, err);
            this.loader.hide();
        })
    }

    imagePreview(input : any) {

        let file   = input.files[0];
        let mime   = file.type.split('/')[0];
        let reader = new FileReader();

        reader.onload = (event : any) => {

            if(mime != 'image') return;

            this.imagePrev = event.target.result;
        };

        if(file && mime == 'image') {
            reader.readAsDataURL(file);
        }
        else {
            this.imagePrev = '';
        }
    }

    removePreview(input : any) {
        this.imagePrev = '';
        $(input).val('');
    }

}
