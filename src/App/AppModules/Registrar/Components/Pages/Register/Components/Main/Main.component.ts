import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RegistrarService} from "../../../../../Registrar.service";
import {GEONUMBER_CATEGORY} from "../../../../../../../EntityModules/GeoNumber/GeoNumber.model";

@Component({
    selector: 'registrar-register-main',
    templateUrl: './Main.component.html',
    styleUrls : ['./Main.component.less']
})

export class RegistrarRegisterMainComponent implements OnInit {

    @ViewChild('modal') private modal : any;
    @ViewChild('loader') private loader : any;
    @ViewChild('userError') private userError : any;
    @ViewChild('errorModal') private errorModal : any;
    @ViewChild('preloader') private preloader : any;

    SEARCH_TYPES = {
        RE : 'REGISTRANT_EMAIL',
        CN : 'CUSTOMER_NUMBER'
    };

    type : string = this.SEARCH_TYPES.RE;

    GEONUMBER_CATEGORY : any = GEONUMBER_CATEGORY;

    search : string = '';

    user : any;

    nameAvailable : boolean = false;

    nameAvailableErr : boolean = false;

    userId : string;
    userEmail : string;

    geoname : any;

    GeomainData : any;

    error : string;

    prices : any = {};

    constructor(
        private RegistrarService : RegistrarService,
        private ActivatedRoute : ActivatedRoute,
        private Router : Router
    ) {
        this.userId = this.ActivatedRoute.snapshot.params['userId'];
        this.userEmail = this.ActivatedRoute.snapshot.queryParams['email'];
        console.log(this.userEmail);
        this.Router.navigate(['/registrar/register']);
        this.setDefaultData();

    }

    setDefaultData() {
        this.GeomainData = {
            category            : this.GEONUMBER_CATEGORY.PERSONAL,
            geomainName         : '',
            geomainNumber       : '',
            language            : 'EN',
            registrantEmail     : this.userEmail || '',
            term                : '1',
        };
    }

    get categoryPrice() {
        if(!this.prices.PGN) return 0;
        let categoryPrice = 0;
        switch(this.GeomainData.category) {
            case this.GEONUMBER_CATEGORY.PERSONAL   : categoryPrice = this.prices.PGN.discountPrice; break;
            case this.GEONUMBER_CATEGORY.BUSINESS   : categoryPrice = this.prices.BGN.discountPrice; break;
            case this.GEONUMBER_CATEGORY.GOVERNMENT : categoryPrice = this.prices.GGN.discountPrice; break;
        }
        return categoryPrice;
    }

    get geomainPrice() {
        if(!this.prices.PGN) return 0;
        let geonamePrice = 0;
        switch(this.GeomainData.geomainName.length) {
            case 2 : geonamePrice = this.prices.SR2.discountPrice; break;
            case 3 : geonamePrice = this.prices.SR3.discountPrice; break;
            case 4 : geonamePrice = this.prices.SR4.discountPrice; break;
            case 5 : geonamePrice = this.prices.SR5.discountPrice; break;
        }
        return geonamePrice;

    }

    get price() {

        if(!this.prices.PGN) return;

        return this.GeomainData.term * (this.categoryPrice + this.geomainPrice);
    }

    cancel() {
        this.user = null;
        this.search = '';
        this.geoname = '';
        this.nameAvailable = false;
        this.nameAvailableErr = false;
        this.setDefaultData();
    }

    ngOnInit() {
        if(this.userId) {
            this.findUserById();
        }
    }

    getPriceList() {
        this.RegistrarService.priceList().subscribe(res => {
            for(let i in res) {
                this.prices[res[i].product] = res[i];
            }
        });
    }

    findUserById() {
        this.preloader.show();
        this.RegistrarService.findUser(this.userId).subscribe(res => {
            this.user = res;
            this.preloader.hide();
            this.onFindUser();
        }, err => {
            this.preloader.hide();
        });
    }

    find() {

        if(!this.search) return;

        this.loader.show();
        this.RegistrarService.search(this.type, this.search).subscribe(res => {
            this.user = res[0];
            this.loader.hide();
            this.onFindUser();
        }, () => {
            this.loader.hide();
        })
    }

    onFindUser() {
        if(!this.user) {
            this.userError.open();
        }
        this.GeomainData.registrantEmail = this.user.email;
    }

    checkGeoname(name : string) {

        if(!name) return;

        this.loader.show();
        this.RegistrarService.checkGeoname(name).subscribe(res => {
            this.nameAvailable = res.success;
            this.GeomainData.geomainName = name;
            this.getPriceList();
            this.loader.hide();
        }, () => {
            this.loader.hide();
            this.nameAvailableErr = true;
        })
    }

    submit() {
        this.loader.show();
        this.RegistrarService.addGeoname(this.GeomainData).subscribe(res => {
            this.geoname = res;
            this.modal.open();
            this.loader.hide();
        }, err => {
            this.loader.hide();
            if(err.message) {
                this.error = err.message;
                this.errorModal.open();
            }
        })
    }



}
