import {Component, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {CODES} from "../../../../AppAuthorization/Components/Registrar/codes";

@Component({
    selector: 'registrar-register',
    templateUrl: './Transfer.component.html',
    styleUrls : ['./Transfer.component.less']
})

export class RegistrarTransferComponent {

    @ViewChild('loader') private loader : any;

    CODES : any = CODES;

    form: FormGroup;

    constructor() {
        this.form = new FormGroup({
            number     : new FormControl(''),
            registrant : new FormControl(''),
            registered : new FormControl(''),
            email      : new FormControl(''),
            phoneCode  : new FormControl(''),
            phoneNum   : new FormControl(''),
            phone      : new FormControl(''),
            url        : new FormControl(''),
            owned      : new FormControl(''),
        });
    }

}
