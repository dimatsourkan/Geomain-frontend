
import {Injectable} from "@angular/core";
import {CRUDService} from "../../BaseClasses/Services/Crud.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import { API_URL, USER_SERVICE_API } from '../../constants';
import {IUser} from "../../EntityModules/User/User.model";


@Injectable()
export class RegistrarService extends CRUDService<any> {

    constructor(protected http : HttpClient) {
        super('registrar', http);
    }

    dashboardSummary(period : any) : Observable<any> {
        return this.http.get(this.setCrudUrl('dashboard-summary'),
            {headers: this.headers, params : { period }}
            )
        .map<any, any>((res) => {
            return {
                amountCreditedGeomains : res.amountCreditedGeomains || 0,
                amountDebited : res.amountDebited || 0,
                countNumberGeomain : res.countNumberGeomain || 0,
                countRegistrants : res.countRegistrants || 0
            }
        })
    }

    createDeposit(amount : any) : Observable<any> {
        return this.http.post(`${API_URL}/registrar/payment/deposit-create-paypal`,
            {}, {headers: this.headers, params : { amount }}
        );
    }

    allTransactions(page : any = 1, size : number = 10) : Observable<any> {
        return this.http.post(this.setCrudUrl('all-transactions'), { page, size }, {headers: this.headers});
    }

    discount() : Observable<any> {
        return this.http.get(`${API_URL}/user/registrar/dashboard-discount`, {headers: this.headers});
    }

    priceList() : Observable<any> {
        return this.http.get(`${API_URL}/registrar/price-list`, {headers: this.headers});
    }

    search(type : string, value : string) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/manage/search?type=${type}&value=${value}`, {headers: this.headers});
    }

    searchCsr(value : string, page : number = 1) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/csr/search`, {
            headers: this.headers,
            params : {value : `${value}`, page : `${page}`, size : '10'}
        });
    }

    getCustomerNumber(geoname : string) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/gname/get-customer-number`, {headers: this.headers, params : {geoname}});
    }

    checkEmail(email : string) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/user/available`, {
            headers: this.headers,
            params : { email }
        });
    }

    createUser(data : any) : Observable<any> {
        return this.http.post(`${API_URL}/registrar/user`, data, {headers: this.headers});
    }

    updateUser(data : IUser) : Observable<any> {
        return this.http.put(`${API_URL}/registrar/user/update/${data.id}`, data, {headers: this.headers});
    }

    findUser(id : string) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/user`, {
            headers: this.headers,
            params : { id }
        });
    }

    checkGeoname(name : string) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/check-availability?name=${name}`, {headers: this.headers});
    }

    addGeoname(data : any) : Observable<any> {
        return this.http.post(`${API_URL}/registrar/gname/registration`, data, {headers: this.headers});
    }

    addSubscription(data : any) : Observable<any> {
        return this.http.post(`${API_URL}/registrar/ad-subscription`, data, {headers: this.headers});
    }

    updateGeoname(data : any) : Observable<any> {
        return this.http.post(`${API_URL}/registrar/gname/update`, data, {headers: this.headers});
    }

    updateExpiry(data : any) : Observable<any> {
        return this.updateGeoname(data);
    }

    getCsrById(id : any) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/csr/${id}`, {headers: this.headers});
    }

    getUserGeonames(customerNumber : any) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/user/geonames`, {headers: this.headers, params : { customerNumber, size : '99' }});
    }

    getGnameSubscriptions(geoname : any) : Observable<any> {
        return this.http.get(`${API_URL}/registrar/gname/subscriptions`, {headers: this.headers, params : { geoname }});
    }

    createCsr(data : any) : Observable<any> {
        return this.http.post(`${API_URL}/registrar/csr`, data, {headers: this.headers});
    }

    updateCsr(csrId : number, data : any) : Observable<any> {
        return this.http.put(`${API_URL}/registrar/csr/${csrId}`, data, {headers: this.headers});
    }

    uploadCsrPhoto(csrId : number, data : any) : Observable<any> {
        return this.http.post(`${USER_SERVICE_API}/registrar/csr/photo/${csrId}`, data, {headers: this.headers});
    }

    deleteCsrPhoto(id : any) : Observable<any> {
        return this.http.delete(`${API_URL}/registrar/csr/photo`, {headers: this.headers, params : {id}});
    }

    deleteCsr(id : any) : Observable<any> {
        return this.http.delete(`${API_URL}/registrar/csr`, {headers: this.headers, params : { id }});
    }

    update(data : any) : Observable<any> {
        return this.http.put(`${API_URL}/registrar/profile`, data, {headers: this.headers});
    }

    paymentCreate(data : any) : Observable<any> {
        return this.http.post(`${API_URL}/registrar/payment/create`, data, {headers: this.headers});
    }
}
