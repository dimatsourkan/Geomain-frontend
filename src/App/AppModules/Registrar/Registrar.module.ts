import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {TranslateModule} from "@ngx-translate/core";
import {LoaderModule} from "../../BaseModules/Loader/Loader.module";
import {AuthorizationModule} from "../../BaseModules/Authorization/Authorization.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ModalModule} from "../../BaseModules/Modal/Modal.module";
import {REGISTRAR_ROUTING} from "./Registrar.routing";
import {FormControlsModule} from "../../Components/FormControls/FormControls.module";
import {RegistrarWrapperComponent} from "./Components/Wrapper/Wrapper.component";
import {RegistrarHeaderComponent} from "./Components/Header/Header.component";
import {RegistrarHeaderLinksComponent} from "./Components/Header/Components/Links/Links.component";
import { RegistrarDashboardComponent } from './Components/Pages/Dashboard/Dashboard.component';
import {RegistrarAccountComponent} from "./Components/Pages/Account/Account.component";
import {RegistrarAccountDashboardComponent} from "./Components/Pages/Account/Components/Dashboard/Dashboard.component";
import {RegistrarAccountAddFundsComponent} from "./Components/Pages/Account/Components/AddFunds/AddFunds.component";
import {RegistrarAccountCustomerCreateComponent} from "./Components/Pages/Account/Components/Customer/Create/Create.component";
import {RegistrarAccountPasswordComponent} from "./Components/Pages/Account/Components/Password/Password.component";
import {RegistrarAccountProfileComponent} from "./Components/Pages/Account/Components/Profile/Profile.component";
import {RegistrarAccountDiscountComponent} from "./Components/Pages/Account/Components/Discount/Discount.component";
import {LineChartModule} from "../../Directives/LineChart/LineChart.module";
import {RegistrarService} from "./Registrar.service";
import {ValidationModule} from "../../BaseModules/Validation/Validation.module";
import {RegistrarRegisterComponent} from "./Components/Pages/Register/Register.component";
import {RegistrarTransferComponent} from "./Components/Pages/Transfer/Transfer.component";
import {RegistrarManageComponent} from "./Components/Pages/Manage/Manage.component";
import {RegistrarManageMainComponent} from "./Components/Pages/Manage/Components/Main/Main.component";
import {RegistrarManageRegistrantRecordComponent} from "./Components/Pages/Manage/Components/RegistrantRecord/RegistrantRecord.component";
import {RegistrarManageGeomainRecordComponent} from "./Components/Pages/Manage/Components/GeomainRecord/GeomainRecord.component";
import {GeoNumberModule} from "../../EntityModules/GeoNumber/GeoNumber.module";
import {RegistrarManageRegistrantCreateComponent} from "./Components/Pages/Manage/Components/CreateRegistrant/CreateRegistrant.component";
import {RegistrarRegisterBulkComponent} from "./Components/Pages/Register/Components/Bulk/Bulk.component";
import {RegistrarRegisterMainComponent} from "./Components/Pages/Register/Components/Main/Main.component";
import {RegistrarRenewComponent} from "./Components/Pages/Renew/Renew.component";
import {RegistrarSupportPageComponent} from "./Components/Pages/Support/Support.component";
import {SupportModule} from "../../BaseModules/Support/Support.module";
import {PaginationModule} from "../../BaseModules/Pagination/Pagination.module";
import {RegistrarAccountCsrComponent} from "./Components/Pages/Account/Components/Csr/Csr.component";
import {RegistrarAccountCustomerUpdateComponent} from "./Components/Pages/Account/Components/Customer/Update/Update.component";
import {RegistrarAccountAddFundsMainComponent} from "./Components/Pages/Account/Components/AddFunds/Components/Main/Main.component";
import {RegistrarAccountAddFundsMethodsComponent} from "./Components/Pages/Account/Components/AddFunds/Components/Methods/Methods.component";
import { RegistrarDashboardMainComponent } from './Components/Pages/Dashboard/Components/Main/Main.component';
import { RegistrarDashboardSubscribtionComponent } from './Components/Pages/Dashboard/Components/Subscribtion/Subscribtion.component';
import { PersonalServiceModule } from '../../Services/PersonalService/PersonalService.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        TranslateModule,
        LoaderModule,
        ValidationModule,
        FormControlsModule,
        LineChartModule,
        GeoNumberModule,

        SupportModule,
        AuthorizationModule,
        ModalModule,
        PersonalServiceModule,
        PaginationModule,

        REGISTRAR_ROUTING
    ],
    declarations: [
        RegistrarRenewComponent,
        RegistrarHeaderComponent,
        RegistrarManageComponent,
        RegistrarWrapperComponent,
        RegistrarAccountComponent,
        RegistrarRegisterComponent,
        RegistrarTransferComponent,
        RegistrarDashboardComponent,
        RegistrarManageMainComponent,
        RegistrarAccountCsrComponent,
        RegistrarSupportPageComponent,
        RegistrarHeaderLinksComponent,
        RegistrarRegisterBulkComponent,
        RegistrarRegisterMainComponent,
        RegistrarDashboardMainComponent,
        RegistrarAccountProfileComponent,
        RegistrarAccountAddFundsComponent,
        RegistrarAccountPasswordComponent,
        RegistrarAccountDiscountComponent,
        RegistrarAccountDashboardComponent,
        RegistrarManageGeomainRecordComponent,
        RegistrarAccountAddFundsMainComponent,
        RegistrarDashboardSubscribtionComponent,
        RegistrarAccountCustomerCreateComponent,
        RegistrarAccountCustomerUpdateComponent,
        RegistrarAccountAddFundsMethodsComponent,
        RegistrarManageRegistrantRecordComponent,
        RegistrarManageRegistrantCreateComponent,
    ],
    exports : [
    ],
    providers: [
        RegistrarService
    ]
})

export class RegistrarModule {

}
