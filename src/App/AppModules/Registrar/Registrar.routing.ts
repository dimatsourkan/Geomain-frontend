import {IsAuthenticated} from "../../BaseModules/Authorization/CanActivate/IsAuthenticated";
import {RouterModule} from "@angular/router";
import {USER_ROLE} from "../../EntityModules/User/User.model";
import {RegistrarWrapperComponent} from "./Components/Wrapper/Wrapper.component";
import { RegistrarDashboardComponent } from './Components/Pages/Dashboard/Dashboard.component';
import {RegistrarAccountComponent} from "./Components/Pages/Account/Account.component";
import {RegistrarAccountDashboardComponent} from "./Components/Pages/Account/Components/Dashboard/Dashboard.component";
import {RegistrarAccountAddFundsComponent} from "./Components/Pages/Account/Components/AddFunds/AddFunds.component";
import {RegistrarAccountCustomerCreateComponent} from "./Components/Pages/Account/Components/Customer/Create/Create.component";
import {RegistrarAccountPasswordComponent} from "./Components/Pages/Account/Components/Password/Password.component";
import {RegistrarAccountProfileComponent} from "./Components/Pages/Account/Components/Profile/Profile.component";
import {RegistrarAccountDiscountComponent} from "./Components/Pages/Account/Components/Discount/Discount.component";
import {RegistrarRegisterComponent} from "./Components/Pages/Register/Register.component";
import {RegistrarTransferComponent} from "./Components/Pages/Transfer/Transfer.component";
import {RegistrarManageComponent} from "./Components/Pages/Manage/Manage.component";
import {RegistrarManageMainComponent} from "./Components/Pages/Manage/Components/Main/Main.component";
import {RegistrarManageRegistrantRecordComponent} from "./Components/Pages/Manage/Components/RegistrantRecord/RegistrantRecord.component";
import {RegistrarManageGeomainRecordComponent} from "./Components/Pages/Manage/Components/GeomainRecord/GeomainRecord.component";
import {RegistrarManageRegistrantCreateComponent} from "./Components/Pages/Manage/Components/CreateRegistrant/CreateRegistrant.component";
import {RegistrarRegisterMainComponent} from "./Components/Pages/Register/Components/Main/Main.component";
import {RegistrarRegisterBulkComponent} from "./Components/Pages/Register/Components/Bulk/Bulk.component";
import {RegistrarRenewComponent} from "./Components/Pages/Renew/Renew.component";
import {RegistrarSupportPageComponent} from "./Components/Pages/Support/Support.component";
import {RegistrarAccountCsrComponent} from "./Components/Pages/Account/Components/Csr/Csr.component";
import {RegistrarAccountCustomerUpdateComponent} from "./Components/Pages/Account/Components/Customer/Update/Update.component";
import {RegistrarAccountAddFundsMainComponent} from "./Components/Pages/Account/Components/AddFunds/Components/Main/Main.component";
import {RegistrarAccountAddFundsMethodsComponent} from "./Components/Pages/Account/Components/AddFunds/Components/Methods/Methods.component";
import { RegistrarDashboardMainComponent } from './Components/Pages/Dashboard/Components/Main/Main.component';
import { RegistrarDashboardSubscribtionComponent } from './Components/Pages/Dashboard/Components/Subscribtion/Subscribtion.component';

export const routes = [
    {
        path: '',
        canActivate : [ IsAuthenticated ],
        component : RegistrarWrapperComponent,
        data : {  roles : [ USER_ROLE.ROLE_REGISTRAR ] },
        children : [
            {
                path : "",
                component : RegistrarDashboardComponent
            },
            {
                path : "dashboard",
                component : RegistrarDashboardComponent,
                children : [
                    {
                        path : "",
                        component : RegistrarDashboardMainComponent
                    },
                    {
                        path : "ad-subscribtion",
                        component : RegistrarDashboardSubscribtionComponent
                    },
                ]
            },
            {
                path : "manage",
                component : RegistrarManageComponent,
                children : [
                    {
                        path : "",
                        component : RegistrarManageMainComponent,
                    },
                    {
                        path : "registrar-record/:id",
                        component : RegistrarManageRegistrantRecordComponent
                    },
                    {
                        path : "registrar-create",
                        component : RegistrarManageRegistrantCreateComponent
                    },
                    {
                        path : ":number",
                        component : RegistrarManageGeomainRecordComponent
                    }
                ]
            },
            {
                path : "register",
                component : RegistrarRegisterComponent,
                children : [
                    {
                        path : "",
                        component : RegistrarRegisterMainComponent
                    },
                    {
                        path : "bulk",
                        component : RegistrarRegisterBulkComponent
                    },
                    {
                        path : ":userId",
                        component : RegistrarRegisterMainComponent
                    },
                ]
            },
            {
                path : "renew",
                component : RegistrarRenewComponent
            },
            {
                path : "transfer",
                component : RegistrarTransferComponent
            },
            {
                path : "support",
                component : RegistrarSupportPageComponent
            },
            {
                path : "account",
                component : RegistrarAccountComponent,
                children : [
                    {
                        path : "",
                        component : RegistrarAccountDashboardComponent,
                        redirectTo : 'dashboard'
                    },
                    {
                        path : "dashboard",
                        component : RegistrarAccountDashboardComponent
                    },
                    {
                        path : "add-funds",
                        component : RegistrarAccountAddFundsComponent,
                        children : [
                            {
                                path : '',
                                component : RegistrarAccountAddFundsMainComponent
                            },
                            {
                                path : 'methods',
                                component : RegistrarAccountAddFundsMethodsComponent
                            }
                        ]
                    },
                    {
                        path : "customer",
                        component : RegistrarAccountCsrComponent
                    },
                    {
                        path : "customer/create",
                        component : RegistrarAccountCustomerCreateComponent
                    },
                    {
                        path : "customer/update/:id",
                        component : RegistrarAccountCustomerUpdateComponent
                    },
                    {
                        path : "password",
                        component : RegistrarAccountPasswordComponent
                    },
                    {
                        path : "profile",
                        component : RegistrarAccountProfileComponent
                    },
                    {
                        path : "discount",
                        component : RegistrarAccountDiscountComponent
                    }
                ]
            }
        ]
    }
];

export const REGISTRAR_ROUTING = RouterModule.forChild(routes);