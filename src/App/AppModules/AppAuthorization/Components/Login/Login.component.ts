import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthorizationService } from "../../../../BaseModules/Authorization/Authorization.service";
import {Helpers} from "../../../../BaseClasses/Helpers";

@Component({
    selector: 'login',
    templateUrl: './Login.component.html',
    styleUrls : ['./Login.component.less']
})

export class LoginComponent {

    Helpers : Helpers = new Helpers();

    form: FormGroup;

    @ViewChild('loader') private loader: any;

    error : boolean = false;

    rememberMe : boolean = false;

    constructor(
        private Router : Router,
        private AuthorizationService : AuthorizationService
    ) {
        this.form = new FormGroup({
            username : new FormControl(''),
            password : new FormControl(''),
            rememberMe : new FormControl(false)
        });

    }

    login() {

        this.loader.show();

        this.error = false;

        this.AuthorizationService
            .login(this.form.value.username, this.form.value.password, this.form.value.rememberMe)
            .subscribe(res => {
                this.AuthorizationService.getProfile().subscribe(res => {
                    this.Router.navigate(['/profile']);
                    this.loader.hide();
                }, err => {
                    this.loader.hide();
                    if(err.error == 'invalid_grant') {
                        this.error = true;
                    }
                });
            }, err => {
                this.loader.hide();
                if(err.error == 'invalid_grant') {
                    this.error = true;
                }
            });

    }
}
