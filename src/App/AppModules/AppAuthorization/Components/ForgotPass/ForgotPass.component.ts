import { Component, ViewChild } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthorizationService} from "../../../../BaseModules/Authorization/Authorization.service";

@Component({
    selector: 'change',
    templateUrl: './ForgotPass.component.html',
    styleUrls : ['./ForgotPass.component.less']
})

export class ForgotPassComponent {

    @ViewChild('loader') private loader : any;
    @ViewChild('modal') private modal : any;
    @ViewChild('error') private error : any;

    form : FormGroup;

    serverError : string = '';

    constructor(private AuthorizationService : AuthorizationService) {
        this.form = new FormGroup({
            email : new FormControl('')
        })
    }

    submit() {
        if(!this.form.value.email) return;
        this.loader.show();
        this.AuthorizationService.forgotPass(this.form.value.email).subscribe(() => {
            this.form.reset();
            this.modal.open();
            this.loader.hide();
        }, err => {
            this.serverError = err.message;
            this.loader.hide();
            this.error.open();
        })
    }
}
