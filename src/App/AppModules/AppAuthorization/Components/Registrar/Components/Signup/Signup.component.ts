import {Component, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from "../../../../../../BaseModules/Authorization/Authorization.service";
import {CODES} from "../../codes";
import {FormControl, FormGroup} from "@angular/forms";
import {ValidatorService} from "../../../../../../BaseModules/Validation/Validation.service";

@Component({
    selector: 'registrar-signup',
    templateUrl: './Signup.component.html',
    styleUrls : ['./Signup.component.less']
})

export class RegistrarSignupComponent {

    @ViewChild('loader') private loader: any;
    @ViewChild('modal') private modal: any;
    @ViewChild('errorModal') private errorModal: any;

    form: FormGroup;

    CODES : any = CODES;

    fileName : string = '';

    modalOptions : any = {
        closeOnConfirm : false,
        closeOnCancel : false,
        closeOnEscape : false,
        closeOnOutsideClick : false
    };

    constructor(
        private Router : Router,
        private ValidatorService : ValidatorService,
        private AuthorizationService : AuthorizationService
    ) {
        this.form = new FormGroup({
            businessName      : new FormControl(''),
            businessRegNumber : new FormControl(''),
            designation       : new FormControl(''),
            firstName         : new FormControl(''),
            lastName          : new FormControl(''),
            geomain           : new FormControl(''),
            email             : new FormControl(''),
            mobilePhone       : new FormControl(''),
            mobilePhoneCode   : new FormControl(''),
            mobilePhoneAll    : new FormControl(''),
            businessPhone     : new FormControl(''),
            businessPhoneCode : new FormControl(''),
            businessPhoneAll  : new FormControl(''),
            password          : new FormControl(''),
            website           : new FormControl(''),
            license           : new FormControl(''),
        });
    }

    uploadFile(file : any) {
        this.fileName = '';
        if(file.files[0]) {
            this.fileName = file.files[0].name;
        }
    }

    submit(formNative : any) {

        this.loader.show();

        this.AuthorizationService.registrarSignUp(this.getFormdata(formNative)).subscribe(() => {
            this.loader.hide();
            this.modal.open();
        }, err => {
            this.ValidatorService.addErrorToForm(this.form, err);
            this.loader.hide();
        });
    }

    getFormdata(formNative : any) {
        let form = new FormData(formNative);
        form.append('businessName',      this.form.value.businessName);
        form.append('businessRegNumber', this.form.value.businessRegNumber);
        form.append('designation',       this.form.value.designation);
        form.append('firstName',         this.form.value.firstName);
        form.append('lastName',          this.form.value.lastName);
        form.append('geomain      ',     this.form.value.geomain.replace(/-/g, ''));
        form.append('email',             this.form.value.email);
        form.append('password',          this.form.value.password);
        form.append('website',           this.form.value.website);
        form.append('mobilePhone', `${this.form.value.mobilePhoneCode}${this.form.value.mobilePhoneAll}`);
        form.append('businessPhone',`${this.form.value.businessPhoneCode}${this.form.value.businessPhoneAll}`);
        return form;
    }

    closeModal() {
        this.modal.close();
        $('body').css('padding-right', 0);
        this.Router.navigate(['/']);
    }
}
