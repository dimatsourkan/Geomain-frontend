import { Component, ViewChild } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthorizationService} from "../../../../BaseModules/Authorization/Authorization.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'change-pass',
    templateUrl: './ChangePass.component.html',
    styleUrls : ['./ChangePass.component.less']
})

export class ChangePassComponent {

    @ViewChild('loader') private loader : any;
    @ViewChild('modal') private modal : any;
    @ViewChild('error') private error : any;

    form : FormGroup;

    serverError : string = '';
    token : string;

    modalOptions : any = {
        closeOnConfirm : false,
        closeOnCancel : false,
        closeOnEscape : false,
        closeOnOutsideClick : false
    };

    constructor(
        private Router : Router,
        private ActivatedRoute : ActivatedRoute,
        private AuthorizationService : AuthorizationService
    ) {
        this.token = this.ActivatedRoute.snapshot.params['token'];
        this.form  = new FormGroup({
            password : new FormControl('')
        })
    }

    submit() {
        if(!this.form.value.password) return;
        this.loader.show();
        this.AuthorizationService.changePass(this.form.value.password, this.token).subscribe(() => {
            this.form.reset();
            this.modal.open();
            this.loader.hide();
        }, err => {
            if(err.fieldErrors) {
                this.serverError = err.fieldErrors[0].message;
            }
            else {
                this.serverError = err.message;
            }
            this.loader.hide();
            this.error.open();
        })
    }

    closeModal() {
        this.modal.close();
        $('body').css('padding-right', 0);
        this.Router.navigate(['/auth', 'login']);
    }
}
