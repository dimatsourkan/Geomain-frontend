import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { AuthorizationService } from "../../../../BaseModules/Authorization/Authorization.service";

@Component({
    selector: 'confirm',
    templateUrl: './Confirm.component.html',
    styleUrls : ['./Confirm.component.less']
})

export class ConfirmComponent implements OnInit {

    @ViewChild('loader') private loader: any;

    constructor(
        private Router : Router,
        private ActivatedRoute : ActivatedRoute,
        private AuthorizationService : AuthorizationService
    ) {}

    ngOnInit() {
        this.loader.show();
        this.AuthorizationService.confirm(this.ActivatedRoute.snapshot.params['token']).subscribe(res => {

            if(res.shouldUpdatePassword) {
                this.Router.navigate(['/'], {queryParams : { should_update_password : res.token }});
            }
            else {
                this.Router.navigate(['/'], {queryParams : { verification_complete : 1 }});
            }
            this.loader.hide();
        }, err => {
            this.loader.hide();
            this.Router.navigate(['/']);
        });
    }
}
