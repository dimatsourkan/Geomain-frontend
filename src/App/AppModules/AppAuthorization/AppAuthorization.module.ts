import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppHttpModule } from '../../BaseModules/Http/Http.module';
import { LoginComponent } from './Components/Login/Login.component';
import { LoaderModule } from '../../BaseModules/Loader/Loader.module';
import { UserModule } from '../../EntityModules/User/User.module';
import {TranslateModule} from "@ngx-translate/core";
import {FormControlsModule} from "../../Components/FormControls/FormControls.module";
import {ConfirmComponent} from "./Components/Confirm/Confirm.component";
import {HttpClientModule} from "@angular/common/http";
import {RegistrarSignupComponent} from "./Components/Registrar/Components/Signup/Signup.component";
import {ModalModule} from "../../BaseModules/Modal/Modal.module";
import {AuthorizationModule} from "../../BaseModules/Authorization/Authorization.module";
import {AUTH_ROUTING} from "./AppAuthorization.routing";
import {ForgotPassComponent} from "./Components/ForgotPass/ForgotPass.component";
import {ChangePassComponent} from "./Components/ChangePass/ChangePass.component";
import {ValidationModule} from "../../BaseModules/Validation/Validation.module";
import {RegistrarLoginComponent} from "./Components/Registrar/Components/Login/Login.component";
import {WrapperModule} from "../../Components/Wrapper/Wrapper.module";

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        CommonModule,
        RouterModule,
        AppHttpModule,
        TranslateModule,
        LoaderModule,
        UserModule,
        ValidationModule,

        AuthorizationModule,
        FormControlsModule,
        WrapperModule,
        ModalModule,

        AUTH_ROUTING
    ],
    declarations: [
        LoginComponent,
        ConfirmComponent,
        ForgotPassComponent,
        ChangePassComponent,
        RegistrarLoginComponent,
        RegistrarSignupComponent
    ],
    exports : [
    ],
    providers: []
})

export class AppAuthorizationModule {}
