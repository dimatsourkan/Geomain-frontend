import {LoginComponent} from "./Components/Login/Login.component";
import {NotAuthenticated} from "../../BaseModules/Authorization/CanActivate/NotAuthenticated";
import {ConfirmComponent} from "./Components/Confirm/Confirm.component";
import {RegistrarSignupComponent} from "./Components/Registrar/Components/Signup/Signup.component";
import {RouterModule} from "@angular/router";
import {ForgotPassComponent} from "./Components/ForgotPass/ForgotPass.component";
import {ChangePassComponent} from "./Components/ChangePass/ChangePass.component";
import {RegistrarLoginComponent} from "./Components/Registrar/Components/Login/Login.component";
import {WrapperComponent} from "../../Components/Wrapper/wrapper.component";

export const routes = [
    {
        path: 'confirm/:token',
        component: ConfirmComponent
    },
    {
        path: '',
        component: WrapperComponent,
        canActivate : [ NotAuthenticated ],
        children : [
            {
                path: '',
                component: LoginComponent
            },
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'forgot-pass',
                component: ForgotPassComponent
            },
            {
                path: 'forgot/:token',
                component: ChangePassComponent
            },
            {
                path: 'registrar-signup',
                component: RegistrarSignupComponent
            },
            {
                path: 'registrar-login',
                component: RegistrarLoginComponent
            },
        ]
    }
];

export const AUTH_ROUTING = RouterModule.forChild(routes);
