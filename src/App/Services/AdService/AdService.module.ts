import { NgModule } from '@angular/core';
import { AdService } from './AdService.service';
import {AppHttpModule} from "../../BaseModules/Http/Http.module";

@NgModule({
    imports: [
        AppHttpModule
    ],
    providers: [
        AdService
    ],
    declarations: [],
    exports: []
})

export class AdServiceModule {

}
