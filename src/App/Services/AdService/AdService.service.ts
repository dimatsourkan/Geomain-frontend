import { Injectable } from '@angular/core';
import { CRUDService } from '../../BaseClasses/Services/Crud.service';
import {HttpClient} from "@angular/common/http";
import {API_URL} from "../../constants";


/**
 * Сервис для работы с персональными данными
 */
@Injectable()
export class AdService extends CRUDService<any> {

    constructor(protected http : HttpClient) {
        super('geomain/ad', http);
    }

    fetchAdsByGeoname(geoname : any) {
        return this.http.get(this.setCrudUrl('fetch-ads-by-geoname'),
                {headers: this.headers, params : { geoname }}
                );
    }

    getAllByBnameId(gNameId : any) {
        return this.http.get(this.setCrudUrl(gNameId),
                {headers: this.headers}
                );
    }

    deleteAdById(id : any) {
        return this.http.delete(this.setCrudUrl(id),{headers: this.headers});
    }

    addVideo(form : any) {
        return this.http
            .post(this.setCrudUrl('vemb'), new FormData(form), {headers: this.headers})
    }

    addBunner(form : any) {
        return this.http
            .post(this.setCrudUrl('banr'), new FormData(form), {headers: this.headers})
    }
}
