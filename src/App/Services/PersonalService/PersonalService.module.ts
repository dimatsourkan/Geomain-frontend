import { NgModule } from '@angular/core';
import { PersonalService } from './PersonalService.service';
import {AppHttpModule} from "../../BaseModules/Http/Http.module";

@NgModule({
    imports: [
        AppHttpModule
    ],
    providers: [
        PersonalService
    ],
    declarations: [],
    exports: []
})

export class PersonalServiceModule {

}
