import { Injectable } from '@angular/core';
import { CRUDService } from '../../BaseClasses/Services/Crud.service';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {IUser, User} from "../../EntityModules/User/User.model";
import { API_URL } from '../../constants';


/**
 * Сервис для работы с персональными данными
 */
@Injectable()
export class PersonalService extends CRUDService<any> {

    constructor(protected http : HttpClient) {
        super('personal', http);
    }

    updateProfile(user : IUser) : Observable<IUser> {
        return this.http
            .put(`${API_URL}/personal/profile`, user, {headers: this.headers})
            .map<any, any>(this.postResponse)
            .map<IUser, any>((res) => new User(res));
    }

    changePass(oldPassword : string, newPassword : string) {
        return this.http
            .put(`${API_URL}/user/password`, { oldPassword, newPassword }, {
                headers: this.headers
            });
    }

    addPostMessage(geomain : any, message : any) {
        return this.http
            .post(`${API_URL}/geomain/feed`, {geomain, message}, {headers: this.headers})
            .map<any, any>(this.postResponse);
    }
}
