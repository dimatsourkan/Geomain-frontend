import { Injectable } from '@angular/core';
import { CRUDService } from '../../BaseClasses/Services/Crud.service';
import {HttpClient} from "@angular/common/http";
import { API_URL, CREDENTIALS } from '../../constants';


/**
 * Сервис для работы с пользователями
 */
@Injectable()
export class OpenService extends CRUDService<any> {

    constructor(protected http : HttpClient) {
        super('open', http);
    }

    /**
     * data = {
            "message": "string",
            "priority": "string",
            "status": "string",
            "topic": "string"
       }
     * @param data
     * @returns {Observable<Object>}
     */
    support(data : any) {
        return this.http.post(this.setCrudUrl('support'), data, {
            headers : this.headers.set('Authorization', `Basic ${btoa(CREDENTIALS)}`)
        });
    }

    whoIs(geomain : any) {
        return this.http.get(`${API_URL}/geomain/who-is`, {
            headers : this.headers.set('Authorization', `Basic ${btoa(CREDENTIALS)}`),
            params : { geomain }
        });
    }
}
