import { NgModule } from '@angular/core';
import { OpenService } from './OpenService.service';
import {AppHttpModule} from "../../BaseModules/Http/Http.module";

@NgModule({
    imports: [
        AppHttpModule
    ],
    providers: [
        OpenService
    ],
    declarations: [],
    exports: []
})

export class OpenServiceModule {

}
