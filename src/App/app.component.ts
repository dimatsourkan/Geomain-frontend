import {AfterViewInit, Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {IGeoNumber} from "./EntityModules/GeoNumber/GeoNumber.model";
import {Helpers} from "./BaseClasses/Helpers";
import {AuthorizationService} from "./BaseModules/Authorization/Authorization.service";

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: [
        './app.component.less'
    ],
    encapsulation: ViewEncapsulation.None
})

export class AppComponent implements AfterViewInit {
    @ViewChild('modal') private modal : any;
    @ViewChild('modalPass') private modalPass : any;

    Helpers : Helpers = new Helpers();

    token : string;

    passError : string;

    modalOptions : any = {
        closeOnConfirm : false,
        closeOnCancel : false,
        closeOnEscape : false,
        closeOnOutsideClick : false
    };

    constructor(
        private Router : Router,
        private ActivatedRoute : ActivatedRoute,
        private AuthorizationService : AuthorizationService
    ){}

    /**
     * Гет параметр будет при удачной оплате
     */
    //checkout_success=true
    ngAfterViewInit() {
        this.ActivatedRoute.queryParams.subscribe(res => {
            if(res.verification_complete) {
                this.Router.navigate(['/']);
                this.modal.open();
            }

            if(res.should_update_password) {
                this.token = res.should_update_password;
                // this.Router.navigate(['/']);
                this.modalPass.open();
            }
        });
    }

    closeModal() {
        this.modal.close();
        this.modalPass.close();
        $('body').css('padding-right', 0);
        this.Router.navigate(['/auth', 'login']);
    }

    submit(pass : string) {

        this.passError = '';

        if(!pass) return false;
        this.AuthorizationService.confirmPass(pass, this.token).subscribe(res => {
            this.closeModal();
            this.Router.navigate(['/'], {queryParams : { verification_complete : 1 }});
        }, err => {
            if(err.fieldErrors) {
                this.passError = err.fieldErrors[0].message;
            }
            else {
                this.passError = err.message;
            }
        });
    }

}
