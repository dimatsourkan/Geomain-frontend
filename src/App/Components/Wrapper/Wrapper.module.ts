import {NgModule} from "@angular/core";
import {WrapperComponent} from "./wrapper.component";
import {RouterModule} from "@angular/router";
import {FooterModule} from "../Footer/Footer.module";
import {HeaderModule} from "../Header/Header.module";
import {AuthorizationModule} from "../../BaseModules/Authorization/Authorization.module";

@NgModule({
    imports: [
        FooterModule,
        HeaderModule,
        RouterModule,
        AuthorizationModule
    ],
    declarations: [
        WrapperComponent
    ],
    exports : [
        WrapperComponent
    ]
})

export class WrapperModule {}