import {Component, Input} from '@angular/core';
import {AuthorizationService} from "../../BaseModules/Authorization/Authorization.service";
import {USER_ROLE} from "../../EntityModules/User/User.model";
import {Router} from "@angular/router";

@Component({
    selector: 'header',
    templateUrl: './Header.component.html',
    styleUrls: ['./Header.component.less']
})

export class HeaderComponent {

    @Input() showButtons : boolean = true;

    nawShowed : boolean = false;

    USER_ROLE : any = USER_ROLE;

    constructor(
        public AuthorizationService : AuthorizationService,
        private Router : Router
    ) {}

    toggleMenu() {
        this.nawShowed = !this.nawShowed;
    }

    logout() {
        this.AuthorizationService.logout();
        this.Router.navigate(['/']);
    }
}
