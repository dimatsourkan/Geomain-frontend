import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HeaderComponent} from "./Header.component";
import {RouterModule} from "@angular/router";
import {DropdownModule} from "ngx-dropdown";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        DropdownModule
    ],
    declarations: [
        HeaderComponent
    ],
    exports : [
        HeaderComponent
    ]
})

export class HeaderModule {}