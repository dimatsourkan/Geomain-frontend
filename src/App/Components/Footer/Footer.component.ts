import {Component} from '@angular/core';

@Component({
    selector: 'footer',
    templateUrl: './Footer.component.html',
    styleUrls: ['./Footer.component.less']
})

export class FooterComponent {}
