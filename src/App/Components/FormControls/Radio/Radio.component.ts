import {Component, forwardRef, Input, ViewChild} from '@angular/core';
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {FormControlsComponent} from "../FormControls.component";

@Component({
    selector: 'radio',
    templateUrl: './Radio.component.html',
    styleUrls:['./Radio.component.less'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RadioComponent),
            multi: true
        }
    ]
})

export class RadioComponent extends FormControlsComponent {

    @ViewChild('input') public input : any;

    @Input()
    checked : boolean = false;

}
