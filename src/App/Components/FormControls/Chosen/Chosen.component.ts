import {Component, forwardRef, Input, ViewChild, ViewEncapsulation} from '@angular/core';
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {FormControlsComponent} from "../FormControls.component";

import * as jQuery from 'jquery';
require('./../../../../Theme/plugins/chosen/chosen.jquery.min.js');

@Component({
    selector: 'chosen',
    templateUrl: './Chosen.component.html',
    styleUrls:['./Chosen.component.less'],
    encapsulation : ViewEncapsulation.None,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ChosenComponent),
            multi: true
        }
    ]
})

export class ChosenComponent extends FormControlsComponent {

    @Input()
    placeholder : string = ' ';

    @Input()
    chosenClass : string = 'chosen-default';

    @Input()
    disabled : boolean = false;

    @Input()
    data : any[];

    $el : any;

    @ViewChild('select') private el : any;

    ngOnInit() {
        this.$el = jQuery(this.el.nativeElement);

        this.$el.chosen({
            width: '100%',
            allow_single_deselect: true
        });

        this.onchange();
    }

    change() {
        this.propagateChange(this.$el.val());
    }

    onchange() {
        this.el.nativeElement.onchange = (e : any) => {
            if(!e.returnValue) {
                this.onChange(this.$el.val());
                let customEvent = document.createEvent('Event');
                customEvent.initEvent('change', true, true);
                this.el.nativeElement.dispatchEvent(customEvent);

            }

        };
    }

    ngOnChanges() {
        setTimeout(() => {
            this.$el.trigger("chosen:updated");
        }, 100);
    }

    writeValue(value: any) {
        this.$el.val(value);
        this.value = value;
        setTimeout(() => {
            this.$el.trigger("chosen:updated");
        }, 100);
    }

    onChange: Function = () => {};

    onTouched: Function = () => {};

}
