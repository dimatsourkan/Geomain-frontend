import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ChosenComponent} from "./Chosen/Chosen.component";
import {CheckboxComponent} from "./Checkbox/Checkbox.component";
import {RadioComponent} from "./Radio/Radio.component";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        RadioComponent,
        ChosenComponent,
        CheckboxComponent
    ],
    exports: [
        RadioComponent,
        ChosenComponent,
        CheckboxComponent
    ]

})

export class FormControlsModule {}

