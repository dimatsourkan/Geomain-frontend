import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ROUTING } from './app.routing';
import { TokenService } from './BaseClasses/Services/Token.service';
import { LoaderModule } from "./BaseModules/Loader/Loader.module";
import {CommonModule} from "@angular/common";
import {MainPageModule} from "./AppModules/MainPage/MainPage.module";
import {WrapperModule} from "./Components/Wrapper/Wrapper.module";
import {HttpClientModule} from "@angular/common/http";
import {ModalModule} from "./BaseModules/Modal/Modal.module";

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        LoaderModule,
        HttpClientModule,
        WrapperModule,
        ModalModule,
        ROUTING,

        MainPageModule
    ],

    declarations: [
        AppComponent
    ],

    providers: [
        TokenService
    ],

    bootstrap: [
        AppComponent
    ]
})

export class AppModule {

    constructor() { }
}