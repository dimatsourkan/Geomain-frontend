import { Injectable } from '@angular/core';
import { CRUDService } from '../../BaseClasses/Services/Crud.service';
import {GeoNumber, IGeoNumber} from './GeoNumber.model';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CREDENTIALS} from "../../constants";

/**
 * Сервис для работы с пользователями
 */
@Injectable()
export class GeoNumberService extends CRUDService<IGeoNumber> {

    private _code :  string = '';

    set Code(code : string) {
        this._code = code;
    }

    constructor(protected http : HttpClient) {
        super('geomain', http);
    }

    getLocation() {
        return this.http.get('http://freegeoip.net/json/')
            .map<any, any>(this.postResponse)
    }

    find(geomain : any) {
        return this.http
            .get(this.setCrudUrl(`find/${geomain}`),
            { headers: this.headers, params : { value : this._code } }
            )
            .map<any, any>(this.postResponse)
            .map<any, any>((res) =>  this.createEntity(res));
    }

    register(data : any) {
        let headers : HttpHeaders = this.headers.set('Authorization', `Basic ${btoa(CREDENTIALS)}`);
        return this.http
            .post(this.setCrudUrl('register'), data, { headers })
            .map<any, any>(this.postResponse)
            .map<any, any>((res) =>  this.createEntity(res));
    }

    search(geomain : string) {
        return this.http
            .get(this.setCrudUrl('search'), { headers: this.headers, params : {geomain} })
            .map<any, any>(this.postResponse)
            .map<any, any>((res) =>  this.createEntity(res));
    }

    createEntity(res : any) {
        res.data = new GeoNumber(res.data[0]);
        return res;
    }
}
