export const VISITOR_POLICY = {
    IVO : {
        clas : 'geopage-drop-warning',
        name : 'Invited Visitors only',
        value : 'IVO'
    },
    FWA : {
        clas : 'geopage-drop-success',
        name : 'Family welcome anytime',
        value : 'FWA'
    },
    FWWE : {
        clas : 'geopage-drop-warning',
        name : 'Family welcome weekday evenings',
        value : 'FWWE'
    },
    FWW : {
        clas : 'geopage-drop-warning',
        name : 'Family welcome weekends',
        value : 'FWW'
    },
    FRWA : {
        clas : 'geopage-drop-success',
        name : 'Friends welcome anytime',
        value : 'FRWA'
    },
    FRWWE : {
        clas : 'geopage-drop-warning',
        name : 'Friends welcome weekday evenings',
        value : 'FRWWE'
    },
    FRWW : {
        clas : 'geopage-drop-warning',
        name : 'Friends welcome weekends',
        value : 'FRWW'
    },
    FFWA : {
        clas : 'geopage-drop-success',
        name : 'Family & Friends welcome anytime',
        value : 'FFWA'
    },
    FFWWE : {
        clas : 'geopage-drop-warning',
        name : 'Family & Friends welcome weekday evenings',
        value : 'FFWWE'
    },
    FFWW : {
        clas : 'geopage-drop-warning',
        name : 'Family & Friends welcome weekends',
        value : 'FFWW'
    },
    NVP : {
        clas : 'geopage-drop-danger',
        name : 'No visitors please',
        value : 'NVP'
    },
    EO : {
        clas : 'geopage-drop-danger',
        name : 'Emergencies only',
        value : 'EO'
    }

};