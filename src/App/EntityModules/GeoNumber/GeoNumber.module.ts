import { NgModule } from '@angular/core';
import { GeoNumberService } from './GeoNumber.service';

@NgModule({
    imports: [
    ],
    providers: [
        GeoNumberService
    ],
    declarations: [],
    exports: []
})

export class GeoNumberModule {

}
