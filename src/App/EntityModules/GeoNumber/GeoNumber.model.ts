import { BaseModel, IModel } from '../../BaseClasses/Models/Base.model';
import {IUser, User} from "../User/User.model";

export const GEONUMBER_CATEGORY = {
    PERSONAL   : 'PERSONAL',
    BUSINESS   : 'BUSINESS',
    GOVERNMENT : 'GOVERNMENT'
};

class GraphicDay {
    timeFrom : string;
    timeTo   : string;
    enabled  : boolean;

    constructor(day : any = {}) {
        this.timeFrom = day.timeFrom || '10:00';
        this.timeTo   = day.timeTo || '18:00';
        this.enabled  = day.enabled || false;
    }
}

export class Graphic {

    MO : GraphicDay = new GraphicDay();
    TU : GraphicDay = new GraphicDay();
    WE : GraphicDay = new GraphicDay();
    TH : GraphicDay = new GraphicDay();
    FR : GraphicDay = new GraphicDay();
    SA : GraphicDay = new GraphicDay();
    SU : GraphicDay = new GraphicDay();

    constructor(graphic : any = {}) {

        if(graphic) {
            this.MO = new GraphicDay(graphic.MO);
            this.TU = new GraphicDay(graphic.TU);
            this.WE = new GraphicDay(graphic.WE);
            this.TH = new GraphicDay(graphic.TH);
            this.FR = new GraphicDay(graphic.FR);
            this.SA = new GraphicDay(graphic.SA);
            this.SU = new GraphicDay(graphic.SU);
        }
    }

}

export interface IGeoNumber extends IModel {
    category       : string;
    created        : number;
    expiryDate     : number;
    lat            : number;
    lon            : number;
    modified       : number;
    number         : number;
    name           : string;
    numberMasked   : string;
    owner          : IUser;
    ownerId        : number;
    premium        : boolean;
    privacyProtect : boolean;
    qrCode         : string;
    qrCodeLink     : string;
    security       : string;
    status         : string;
    type           : string;
    visitorPolicy  : string;
    messages       : any;
    website        : string;
    openTime       : string;
    about          : string;
    tag            : string;

    checked        : boolean;
    getGraphic()   : Graphic;
}

export class GeoNumber extends BaseModel implements IGeoNumber {

    category       : string;
    created        : number;
    expiryDate     : number;
    lat            : number;
    lon            : number;
    modified       : number;
    number         : number;
    name           : string;
    owner          : IUser;
    ownerId        : number;
    premium        : boolean;
    privacyProtect : boolean;
    qrCode         : string;
    qrCodeLink     : string;
    security       : string;
    status         : string;
    type           : string;
    visitorPolicy  : string;
    messages       : any;
    website        : string;
    openTime       : string;
    about          : string = '';
    tag            : string = '';

    checked        : boolean;

    constructor(model ?: any) {
        super(model);
        if (model) {
            this.category       = model.category        || null ;
            this.created        = model.created         || null ;
            this.expiryDate     = model.expiryDate      || null;
            this.lat            = model.lat             || null ;
            this.lon            = model.lon             || null ;
            this.modified       = model.modified        || null;
            this.number         = model.number          || null ;
            this.name           = model.name            || null ;
            this.owner          = new User(model.owner)         ;
            this.premium        = model.premium         || false ;
            this.privacyProtect = model.privacyProtect  || false ;
            this.qrCode         = model.qrCode          || null ;
            this.ownerId        = model.ownerId         || null ;
            this.qrCodeLink     = model.qrCodeLink      || null ;
            this.security       = model.security        || null ;
            this.status         = model.status          || null ;
            this.type           = model.type            || null ;
            this.visitorPolicy  = model.visitorPolicy   || null ;
            this.messages       = model.messages        || null ;
            this.checked        = model.checked         || false ;
            this.website        = model.website         || null ;
            this.openTime       = model.openTime        || null ;
        }
    }

    get numberMasked() : string {
        let masked : string = '';

        if(this.number) {
            let num : string = this.number.toString();
            for(let i=1; i <= num.length; i++) {
                masked += num[i-1];
                if(i % 4 == 0) {
                   masked += '-';
                }
            }
        }

        return masked;
    }

    getGraphic() {
        let data = '';
        try {
            data = JSON.parse(this.openTime)
        }
        catch(err) {

        }
        return new Graphic(data);
    }
}