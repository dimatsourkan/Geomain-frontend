import { BaseModel, IModel } from '../../BaseClasses/Models/Base.model';

export const USER_ROLE = {
    ROLE_PERSONAL  : 'ROLE_PERSONAL',
    ROLE_REGISTRAR : 'ROLE_REGISTRAR',
    ROLE_CSR       : 'ROLE_CSR',
    ROLE_FIREBASE  : 'ROLE_FIREBASE'
};

export interface IUser extends IModel {
    firstName : string;
    lastName  : string;
    photoLink : string;
    email     : string;
    csrId     : string;
    role      : string;

    balance           : number;
    geomain           : string;
    arid              : string;
    businessName      : string;
    businessPhone     : string;
    businessRegNumber : string;
    designation       : string;
    licenseImageUrl   : string;
    website           : string;
    discountPlan      : string;
    fullName : string;

}

export class User extends BaseModel implements IUser {

    firstName : string;
    lastName  : string;
    photoLink : string;
    email     : string;
    csrId     : string;
    role      : string;

    balance           : number;
    geomain           : string;
    arid              : string;
    businessName      : string;
    businessPhone     : string;
    businessRegNumber : string;
    designation       : string;
    licenseImageUrl   : string;
    website           : string;
    discountPlan      : string;

    constructor(model ?: any) {
        super(model);
        if (model) {
            this.firstName = model.firstName || null;
            this.lastName  = model.lastName  || null;
            this.photoLink = model.photoLink || null;
            this.email     = model.email     || null;
            this.csrId     = model.csrId     || null;
            this.role      = model.role      || null;

            this.arid              = model.arid              || null;
            this.balance           = model.balance           || null;
            this.geomain           = model.geomain           || null;
            this.businessName      = model.businessName      || null;
            this.businessPhone     = model.businessPhone     || null;
            this.businessRegNumber = model.businessRegNumber || null;
            this.designation       = model.designation       || null;
            this.licenseImageUrl   = model.licenseImageUrl   || null;
            this.website           = model.website           || null;
            this.discountPlan      = model.discountPlan      || null;
        }
    }

    get fullName() : string {
        return this.firstName+' '+this.lastName;
    }
}