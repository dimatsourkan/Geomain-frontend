import { NgModule } from '@angular/core';
import { UserService } from './Users.service';
import {AppHttpModule} from "../../BaseModules/Http/Http.module";

@NgModule({
    imports: [
        AppHttpModule
    ],
    providers: [
        UserService
    ],
    declarations: [],
    exports: []
})

export class UserModule {

}
