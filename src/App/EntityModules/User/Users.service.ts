import { Injectable } from '@angular/core';
import { CRUDService } from '../../BaseClasses/Services/Crud.service';
import { IUser, User } from './User.model';
import { IResult } from '../../BaseClasses/Models/Result.model';
import { Observable } from 'rxjs/Observable';
import {GeoNumber, IGeoNumber} from "../GeoNumber/GeoNumber.model";
import {HttpClient} from "@angular/common/http";
import { API_URL, USER_SERVICE_API } from '../../constants';


/**
 * Сервис для работы с пользователями
 */
@Injectable()
export class UserService extends CRUDService<IUser> {

    constructor(protected http : HttpClient) {
        super('user', http);
    }

    /**
     * Получение данных профиля
     * @returns {Observable<IResult<IUser>>}
     */
    getProfile() : Observable<IUser> {
        return this.http
            .get(`${API_URL}/user/profile`, {headers: this.headers})
            .map<any, any>(this.postResponse)
            .map<IUser, IUser>((res) => this.createEntity(res));
    }

    uploadAvatar(form : any) {
        return this.http
            .post(`${USER_SERVICE_API}/user/photo`, new FormData(form), {headers: this.headers})
            .map<any, any>(this.postResponse)
            .map<IUser, IUser>((res) => this.createEntity(res));
    }

    deleteAvatar() : Observable<any> {
        return this.http
            .delete(`${API_URL}/user/photo`, {headers: this.headers})
    }

    getGeomains() {
        return this.http
            .get(`${API_URL}/geomain/geomains`, { headers: this.headers })
            .map<any, any>(this.postResponse)
            .map<IGeoNumber[], IGeoNumber[]>((res) => {
                return res.map((geonumber : IGeoNumber) => {
                    return new GeoNumber(geonumber);
                })
            })
    }

    deleteGeomain(geomain : IGeoNumber) {
        return this.http
            .delete(`${this.setCrudUrl('delete-geo-number')}?name=${geomain.number}`, { headers: this.headers });
    }

    updateGeomain(geomain : IGeoNumber) {
        return this.http
            .put(`${API_URL}/geomain/${geomain.name}`, geomain, {headers: this.headers});
    }

    updatePolicy(geomain : any, visitorPolicy : string) {
        return this.http
            .put( `${API_URL}/geomain/visitor-policy`, {}, {
                headers: this.headers,
            params : { geomain, visitorPolicy }
            });
    }

    updateDpp(geomain : any, value : any = false) {
        return this.http
            .put(`${API_URL}/geomain/dpp`, {}, {
                headers: this.headers,
                params : { geomain, value }
            });
    }

    linkById(geonameIds : any[], geonumberId : any) {
        return this.http
            .post(`${API_URL}/geoname/assign-gNames-to-gNumber-by-id`, {geonameIds, geonumberId}, {headers: this.headers});
    }

    linkByNumber(geonameIds : any[], geonumber : any) {
        return this.http
            .post(`${API_URL}/geoname/assign-gNames-to-gNumber-by-number`, {geonameIds, geonumber}, {headers: this.headers});
    }

    updateGeomainSeq(data : any) {
        return this.http
            .put( `${API_URL}/geomain/security`, data, {headers: this.headers});
    }

    createEntity(entity: IUser): IUser {
        return new User(entity);
    }
}
